/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of thnpm e License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
// Configure the email transport using the default SMTP transport and a GMail account.
// For Gmail, enable these:
// 1. https://www.google.com/settings/security/lesssecureapps
// 2. https://accounts.google.com/DisplayUnlockCaptcha
// For other types of transports such as Sendgrid see https://nodemailer.com/transports/
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: gmailEmail,
		pass: gmailPassword
	}
});

// Your company name to include in the emails
const APP_NAME = 'Shipper';

// [START sendSubscribeEmail]
/**
 * Enviar correo de subscripcion
 */
// [START onCreateTrigger]
exports.sendSubscribeEmail = functions.database.ref('/contacto/newsletter/{newsletterId}').onWrite(event => {
// [END onCreateTrigger]
// 	// [START eventAttributes]
//   var keys = [];
// 	const email = Object.keys(event.data); // The Firebase user.
//   Object.keys(event.data).forEach(function(key) {
//     keys.push(key + ': ' + event.data[key]);
//   });
  const email = event.data.val();

  // const email = user.email; // The Firebase user.
	// [END eventAttributes]
	return sendSubscribeEmail(email);
});
// [END sendSubscribeEmail]
/**
 * Sends a welcome email to new user.
 */
// [START onCreateTrigger]
exports.sendWelcomeEmail = functions.auth.user().onCreate(event => {
// [END onCreateTrigger]
  // [START eventAttributes]
  const user = event.data; // The Firebase user.

  const email = user.email; // The email of the user.
  //const displayName = user.name; // The display name of the user.
  // [END eventAttributes]

  return sendWelcomeEmail(email);
});
// [END sendWelcomeEmail]
// [START sendByeEmail]
/**
 * Send an account deleted email confirmation to users who delete their accounts.
 */
// [START onDeleteTrigger]
exports.sendByeEmail = functions.auth.user().onDelete(event => {
// [END onDeleteTrigger]
	const user = event.data;

	const email = user.email;
	const displayName = user.displayName;

	return sendGoodbyEmail(email, displayName);
});
// [END sendByeEmail]


// Sends a newsletter email to the given user.
function sendSubscribeEmail(email) {
  const mailOptions = {
    from: `${APP_NAME} <noreply@firebase.com>`,
    to: email
  };
  // The user unsubscribed to the newsletter.
  mailOptions.subject = `Disfruta de nuestras promociones, ${email || ''}!`;
  mailOptions.text = `Confirmamos que has subscrito tu cuenta a la seccion informativa de ${APP_NAME}.`;
  mailOptions.html = `<!DOCTYPE html> 
      <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"> 
      <head> 
          <meta charset="utf-8"> <!-- utf-8 works for most cases --> 
          <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn\\\\t be necessary --> 
          <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine --> 
          <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely --> 
          <title></title> <!-- The title tag shows in email notifications, like Android 4.4. --> 
       
          <!-- Web Font / @font-face : BEGIN --> 
          <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. --> 
       
          <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. --> 
          <!--[if mso]> 
              <style> 
                  * { 
                      font-family: sans-serif !important; 
                  } 
              </style> 
          <![endif]--> 
       
          <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ --> 
          <!--[if !mso]><!--> 
          <!-- insert web font reference, eg: <link href=\\\\https://fonts.googleapis.com/css?family=Roboto:400,700\\\\ rel=\\\\stylesheet\\\\ type=\\\\text/css\\\\> --> 
          <!--<![endif]--> 
       
          <!-- Web Font / @font-face : END --> 
       
          <!-- CSS Reset : BEGIN --> 
          <style> 
       
              /* What it does: Remove spaces around the email design added by some email clients. */ 
              /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */ 
              html, 
              body { 
                  margin: 0 auto !important; 
                  padding: 0 !important; 
                  height: 100% !important; 
                  width: 100% !important; 
              } 
       
              /* What it does: Stops email clients resizing small text. */ 
              * { 
                  -ms-text-size-adjust: 100%; 
                  -webkit-text-size-adjust: 100%; 
              } 
       
              /* What it does: Centers email on Android 4.4 */ 
              div[style*="margin: 16px 0"] { 
                  margin: 0 !important; 
              } 
       
              /* What it does: Stops Outlook from adding extra spacing to tables. */ 
              table, 
              td { 
                  mso-table-lspace: 0pt !important; 
                  mso-table-rspace: 0pt !important; 
              } 
       
              /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */ 
              table { 
                  border-spacing: 0 !important; 
                  border-collapse: collapse !important; 
                  table-layout: fixed !important; 
                  margin: 0 auto !important; 
              } 
              table table table { 
                  table-layout: auto; 
              } 
       
              /* What it does: Uses a better rendering method when resizing images in IE. */ 
              img { 
                  -ms-interpolation-mode:bicubic; 
              } 
       
              /* What it does: A work-around for email clients meddling in triggered links. */ 
              *[x-apple-data-detectors],  /* iOS */ 
              .x-gmail-data-detectors,    /* Gmail */ 
              .x-gmail-data-detectors *, 
              .aBn { 
                  border-bottom: 0 !important; 
                  cursor: default !important; 
                  color: inherit !important; 
                  text-decoration: none !important; 
                  font-size: inherit !important; 
                  font-family: inherit !important; 
                  font-weight: inherit !important; 
                  line-height: inherit !important; 
              } 
       
              /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */ 
              .a6S { 
                 display: none !important; 
                 opacity: 0.01 !important; 
             } 
             /* If the above doesn\\\\t work, add a .g-img class to any image in question. */ 
             img.g-img  div { 
                 display: none !important; 
             } 
       
             /* What it does: Prevents underlining the button text in Windows 10 */ 
              .button-link { 
                  text-decoration: none !important; 
              } 
       
              /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */ 
              /* Create one of these media queries for each additional viewport size you\\\\d like to fix */ 
              /* Thanks to Eric Lepetit (@ericlepetitsf) for help troubleshooting */ 
              @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6 */ 
                  .email-container { 
                      min-width: 375px !important; 
                  } 
              } 
       
          </style> 
          <!-- CSS Reset : END --> 
       
          <!-- Progressive Enhancements : BEGIN --> 
          <style> 
       
              /* What it does: Hover styles for buttons */ 
              .button-td, 
              .button-a { 
                  transition: all 100ms ease-in; 
              } 
              .button-td:hover, 
              .button-a:hover { 
                  background: #555555 !important; 
                  border-color: #555555 !important; 
              } 
       
              /* Media Queries */ 
              @media screen and (max-width: 600px) { 
       
                  .email-container { 
                      width: 100% !important; 
                      margin: auto !important; 
                  } 
       
                  /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */ 
                  .fluid { 
                      max-width: 100% !important; 
                      height: auto !important; 
                      margin-left: auto !important; 
                      margin-right: auto !important; 
                  } 
       
                  /* What it does: Forces table cells into full-width rows. */ 
                  .stack-column, 
                  .stack-column-center { 
                      display: block !important; 
                      width: 100% !important; 
                      max-width: 100% !important; 
                      direction: ltr !important; 
                  } 
                  /* And center justify these ones. */ 
                  .stack-column-center { 
                      text-align: center !important; 
                  } 
       
                  /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */ 
                  .center-on-narrow { 
                      text-align: center !important; 
                      display: block !important; 
                      margin-left: auto !important; 
                      margin-right: auto !important; 
                      float: none !important; 
                  } 
                  table.center-on-narrow { 
                      display: inline-block !important; 
                  } 
       
                  /* What it does: Adjust typography on small screens to improve readability */ 
                  .email-container p { 
                      font-size: 17px !important; 
                      line-height: 22px !important; 
                  } 
              } 
       
          </style> 
          <!-- Progressive Enhancements : END --> 
       
          <!-- What it does: Makes background images in 72ppi Outlook render at correct size. --> 
          <!--[if gte mso 9]> 
          <xml> 
              <o:OfficeDocumentSettings> 
                  <o:AllowPNG/> 
                  <o:PixelsPerInch>96</o:PixelsPerInch> 
              </o:OfficeDocumentSettings> 
          </xml> 
          <![endif]--> 
       
      </head> 
      <body width="100%" bgcolor="#222222" style="margin: 0; mso-line-height-rule: exactly;"> 
          <center style="width: 100%; background: #222222; text-align: left;"> 
       
              <!-- Visually Hidden Preheader Text : BEGIN --> 
              <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;"> 
                  Bienvenido! Ingresa a Shipper ahora, esperamos por ti.
              </div> 
              <!-- Visually Hidden Preheader Text : END --> 
       
              <!-- Email Header : BEGIN --> 
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container"> 
                  <tr> 
                      <td style="padding: 20px 0; text-align: center"> 
                          <img src="https://vignette.wikia.nocookie.net/clubpenguin/images/7/7e/Tim%C3%B3n_del_Capit%C3%A1n_icono.png/revision/latest?cb=20141115182714&path-prefix=es" width="200" height="50" alt="alt_text" border="0" style="height: auto; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;"></td> 
                  </tr> 
              </table> 
              <!-- Email Header : END --> 
       
              <!-- Email Body : BEGIN --> 
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container"> 
       
                  <!-- Hero Image, Flush : BEGIN --> 
                  
                  <!-- Hero Image, Flush : END --> 
       
                  <!-- 1 Column Text  Button : BEGIN --> 
                  <tr> 
                      <td bgcolor="#ffffff" style="padding: 40px 40px 20px; text-align: center;"> 
                          <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #333333; font-weight: normal;">¡Gracias por registrarte al <br> boletin informativo de  Shipper!</h1> 
                      </td> 
                    
                  </tr> 
              <tr>
<td style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" bgcolor="#ffffff">
<table style="margin: auto;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="button-td" style="border-radius: 0px; background: #222222; text-align: center;"><a class="button-a" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" href="https://shipper-fb694.firebaseapp.com/#/products/"> &nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #ffffff;">Ver todos los productos! </span>&nbsp;&nbsp;&nbsp;&nbsp; </a></td>
</tr>
</tbody>
</table>
  <br><table style="margin: auto;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="button-td" style="border-radius: px; background: #222222; text-align: center;"><a class="button-a" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" href="https://shipper-fb694.firebaseapp.com/#/products/-KsHS00YbB6HEN3GxpEJ"> <span style="color: #ffffff;">Tornillos</span><br><br><img src="https://media.midwayusa.com/productimages/880x660/Primary/743/743416.jpg" width="50%" height="50%  alt="Mountain View"">&nbsp;&nbsp;&nbsp;&nbsp; </a></td>
 <td class="button-td" style="border-radius: 0px; background: #222222; text-align: center;"><a class="button-a" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" href="https://shipper-fb694.firebaseapp.com/#/products/-KsHSsvMsD34PA6qjNBn"> <span style="color: #ffffff;">Plantas</span><br><br><img src="https://previews.123rf.com/images/sspopov/sspopov1211/sspopov121100039/16326469-Standby-generator-electric-power-plant-isolated-Stock-Photo-generator-diesel-engine.jpg" width="40%" height="20%  alt="Mountain View""></a></td>
</tr>
 <tr>
<td class="button-td" style="border-radius: 0px; background: #222222; text-align: center;"><a class="button-a" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" href="https://shipper-fb694.firebaseapp.com/#/products/-KsKvtYhXeCGEq2st2Eh"> <span style="color: #ffffff;">Compresores</span><br><br><img src="http://truckdrivetrain.co/wp-content/uploads/2012/12/Placeholder-for-Hybrid-trans.jpg" width="65%" height="50%  alt="Mountain View""> </span>&nbsp;&nbsp;&nbsp;&nbsp; </a></td>
  
  <td class="button-td" style="border-radius: 3px; background: #222222; text-align: center;"><a class="button-a" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" href="https://shipper-fb694.firebaseapp.com/#/products/-KsWZhoTbPy3TbyjfJ63"> <span style="color: #ffffff;">Arandelas</span><br><br><img src="https://i.ebayimg.com/images/a/(KGrHqJ,!nkE63U,eukNBO3-HZdn,w~~/s-l300.jpg" width="35%" height="50%  alt="Mountain View""> </a></td>
</tr> 
</tbody>
</table>
<!-- Button : END --></td>
</tr>
                  <tr> 
                      <td bgcolor="#ffffff" style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;"> 
                          <p style="margin: 0;">Contamos con el respaldo de reconocidas firmas a nivel mundial, establecidos a través de convenios comerciales que hacen de nosotros, la empresa líder por excelencia en el ramo marítimo venezolano.</p> 
                      </td> 
                  </tr> 
          </table> 
          <!-- Email Body : END --> 
       
          <!-- Email Footer : BEGIN --> 
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto; font-family: sans-serif; color: #888888; line-height:18px;" class="email-container"> 
              <tr> 
                  <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;" class="x-gmail-data-detectors"> 
                             
                      <br><br> 
                  </td>
                
              </tr> 
            
          </table> 
          <!-- Email Footer : END --> 
       
          <!-- Full Bleed Background Section : BEGIN --> 
          <table role="presentation" bgcolor="#709f2b" cellspacing="0" cellpadding="0" border="0" align="center" width="100%"> 
              <tr> 
                  <td valign="top" align="center"> 
                      <div style="max-width: 600px; margin: auto;" class="email-container"> 
                          <!--[if mso]> 
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center"> 
                          <tr> 
                          <td> 
                          <![endif]--> 
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"> 
                              <tr> 
                                  <td style="padding: 40px; text-align: left; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #ffffff;"> 
                                       Ubicación: Zona Industrial II Etapa, PI-4, Av. 67A, entre calles <br>149A y 149B. San Francisco - Edo. Zulia. Venezuela<br> 
																			Teléfono: 58- 261-7362575<br> 
																			Correos electrónicos:<br> 
																			 j.tamayo@sistemasdepropulsion.com<br> 
																			 m.fuentes@sistemasdepropulsion.com </td> 
                              </tr> 
                          </table> 
                          <!--[if mso]> 
                          </td> 
                          </tr> 
                          </table> 
                          <![endif]--> 
                      </div> 
                  </td> 
              </tr> 
          </table> 
          <!-- Full Bleed Background Section : END --> 
       
          </center>\\\\n 
      </body>\\\\n 
      </html>`;
  return mailTransport.sendMail(mailOptions).then(() => {
    console.log('Nuevo email subscrito al newsletter:', email);
  });
}
// Sends a welcome email to the given user.
function sendWelcomeEmail(email) {
	const mailOptions = {
		from: `${APP_NAME} <noreply@firebase.com>`,
		to: email
	};

	// The user subscribed to the newsletter.
	mailOptions.subject = `Hey ${email || ''}! Te damos la bienvenida a  Shipper.`;
	mailOptions.text = `Esperamos que disfrutes de nuestros servicios.`;
	mailOptions.html = `<!DOCTYPE html> 
      <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"> 
      <head> 
          <meta charset="utf-8"> <!-- utf-8 works for most cases --> 
          <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn\\t be necessary --> 
          <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine --> 
          <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely --> 
          <title></title> <!-- The title tag shows in email notifications, like Android 4.4. --> 
       
          <!-- Web Font / @font-face : BEGIN --> 
          <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. --> 
       
          <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. --> 
          <!--[if mso]> 
              <style> 
                  * { 
                      font-family: sans-serif !important; 
                  } 
              </style> 
          <![endif]--> 
       
          <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ --> 
          <!--[if !mso]><!--> 
          <!-- insert web font reference, eg: <link href=\\https://fonts.googleapis.com/css?family=Roboto:400,700\\ rel=\\stylesheet\\ type=\\text/css\\> --> 
          <!--<![endif]--> 
       
          <!-- Web Font / @font-face : END --> 
       
          <!-- CSS Reset : BEGIN --> 
          <style> 
       
              /* What it does: Remove spaces around the email design added by some email clients. */ 
              /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */ 
              html, 
              body { 
                  margin: 0 auto !important; 
                  padding: 0 !important; 
                  height: 100% !important; 
                  width: 100% !important; 
              } 
       
              /* What it does: Stops email clients resizing small text. */ 
              * { 
                  -ms-text-size-adjust: 100%; 
                  -webkit-text-size-adjust: 100%; 
              } 
       
              /* What it does: Centers email on Android 4.4 */ 
              div[style*="margin: 16px 0"] { 
                  margin: 0 !important; 
              } 
       
              /* What it does: Stops Outlook from adding extra spacing to tables. */ 
              table, 
              td { 
                  mso-table-lspace: 0pt !important; 
                  mso-table-rspace: 0pt !important; 
              } 
       
              /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */ 
              table { 
                  border-spacing: 0 !important; 
                  border-collapse: collapse !important; 
                  table-layout: fixed !important; 
                  margin: 0 auto !important; 
              } 
              table table table { 
                  table-layout: auto; 
              } 
       
              /* What it does: Uses a better rendering method when resizing images in IE. */ 
              img { 
                  -ms-interpolation-mode:bicubic; 
              } 
       
              /* What it does: A work-around for email clients meddling in triggered links. */ 
              *[x-apple-data-detectors],  /* iOS */ 
              .x-gmail-data-detectors,    /* Gmail */ 
              .x-gmail-data-detectors *, 
              .aBn { 
                  border-bottom: 0 !important; 
                  cursor: default !important; 
                  color: inherit !important; 
                  text-decoration: none !important; 
                  font-size: inherit !important; 
                  font-family: inherit !important; 
                  font-weight: inherit !important; 
                  line-height: inherit !important; 
              } 
       
              /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */ 
              .a6S { 
                 display: none !important; 
                 opacity: 0.01 !important; 
             } 
             /* If the above doesn\\t work, add a .g-img class to any image in question. */ 
             img.g-img  div { 
                 display: none !important; 
             } 
       
             /* What it does: Prevents underlining the button text in Windows 10 */ 
              .button-link { 
                  text-decoration: none !important; 
              } 
       
              /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */ 
              /* Create one of these media queries for each additional viewport size you\\d like to fix */ 
              /* Thanks to Eric Lepetit (@ericlepetitsf) for help troubleshooting */ 
              @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6 */ 
                  .email-container { 
                      min-width: 375px !important; 
                  } 
              } 
       
          </style> 
          <!-- CSS Reset : END --> 
       
          <!-- Progressive Enhancements : BEGIN --> 
          <style> 
       
              /* What it does: Hover styles for buttons */ 
              .button-td, 
              .button-a { 
                  transition: all 100ms ease-in; 
              } 
              .button-td:hover, 
              .button-a:hover { 
                  background: #555555 !important; 
                  border-color: #555555 !important; 
              } 
       
              /* Media Queries */ 
              @media screen and (max-width: 600px) { 
       
                  .email-container { 
                      width: 100% !important; 
                      margin: auto !important; 
                  } 
       
                  /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */ 
                  .fluid { 
                      max-width: 100% !important; 
                      height: auto !important; 
                      margin-left: auto !important; 
                      margin-right: auto !important; 
                  } 
       
                  /* What it does: Forces table cells into full-width rows. */ 
                  .stack-column, 
                  .stack-column-center { 
                      display: block !important; 
                      width: 100% !important; 
                      max-width: 100% !important; 
                      direction: ltr !important; 
                  } 
                  /* And center justify these ones. */ 
                  .stack-column-center { 
                      text-align: center !important; 
                  } 
       
                  /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */ 
                  .center-on-narrow { 
                      text-align: center !important; 
                      display: block !important; 
                      margin-left: auto !important; 
                      margin-right: auto !important; 
                      float: none !important; 
                  } 
                  table.center-on-narrow { 
                      display: inline-block !important; 
                  } 
       
                  /* What it does: Adjust typography on small screens to improve readability */ 
                  .email-container p { 
                      font-size: 17px !important; 
                      line-height: 22px !important; 
                  } 
              } 
       
          </style> 
          <!-- Progressive Enhancements : END --> 
       
          <!-- What it does: Makes background images in 72ppi Outlook render at correct size. --> 
          <!--[if gte mso 9]> 
          <xml> 
              <o:OfficeDocumentSettings> 
                  <o:AllowPNG/> 
                  <o:PixelsPerInch>96</o:PixelsPerInch> 
              </o:OfficeDocumentSettings> 
          </xml> 
          <![endif]--> 
       
      </head> 
      <body width="100%" bgcolor="#222222" style="margin: 0; mso-line-height-rule: exactly;"> 
          <center style="width: 100%; background: #222222; text-align: left;"> 
       
              <!-- Visually Hidden Preheader Text : BEGIN --> 
              <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;"> 
                  Bienvenido! Ingresa a Shipper ahora, esperamos por ti.
              </div> 
              <!-- Visually Hidden Preheader Text : END --> 
       
              <!-- Email Header : BEGIN --> 
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container"> 
                  <tr> 
                      <td style="padding: 20px 0; text-align: center"> 
                          <img src="https://vignette.wikia.nocookie.net/clubpenguin/images/7/7e/Tim%C3%B3n_del_Capit%C3%A1n_icono.png/revision/latest?cb=20141115182714&path-prefix=es" width="200" height="50" alt="alt_text" border="0" style="height: auto; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;"></td> 
                  </tr> 
              </table> 
              <!-- Email Header : END --> 
       
              <!-- Email Body : BEGIN --> 
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container"> 
       
                  <!-- Hero Image, Flush : BEGIN --> 
                  
                  <!-- Hero Image, Flush : END --> 
       
                  <!-- 1 Column Text  Button : BEGIN --> 
                  <tr> 
                      <td bgcolor="#ffffff" style="padding: 40px 40px 20px; text-align: center;"> 
                          <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #333333; font-weight: normal;">¡Bienvenido a Shipper ${email}!.</h1> 
                      </td> 
                    
                  </tr> 
              <tr>
<td style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" bgcolor="#ffffff">
<table style="margin: auto;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="button-td" style="border-radius: 3px; background: #222222; text-align: center;"><a class="button-a" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" href="https://shipper-fb694.firebaseapp.com/#/users/login/"> &nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #ffffff;">Ir a Shipper</span>&nbsp;&nbsp;&nbsp;&nbsp; </a></td>
</tr>
</tbody>
</table>
<!-- Button : END --></td>
</tr>
                  <tr> 
                      <td bgcolor="#ffffff" style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;"> 
                          <p style="margin: 0;">Contamos con el respaldo de reconocidas firmas a nivel mundial, establecidos a través de convenios comerciales que hacen de nosotros, la empresa líder por excelencia en el ramo marítimo venezolano.</p> 
                      </td> 
                  </tr> 
          </table> 
          <!-- Email Body : END --> 
       
          <!-- Email Footer : BEGIN --> 
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto; font-family: sans-serif; color: #888888; line-height:18px;" class="email-container"> 
              <tr> 
                  <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;" class="x-gmail-data-detectors"> 
                             
                      <br><br> 
                  </td>
                
              </tr> 
            
          </table> 
          <!-- Email Footer : END --> 
       
          <!-- Full Bleed Background Section : BEGIN --> 
          <table role="presentation" bgcolor="#709f2b" cellspacing="0" cellpadding="0" border="0" align="center" width="100%"> 
              <tr> 
                  <td valign="top" align="center"> 
                      <div style="max-width: 600px; margin: auto;" class="email-container"> 
                          <!--[if mso]> 
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center"> 
                          <tr> 
                          <td> 
                          <![endif]--> 
                          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"> 
                              <tr> 
                                  <td style="padding: 40px; text-align: left; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #ffffff;"> 
                                       Ubicación: Zona Industrial II Etapa, PI-4, Av. 67A, entre calles <br>149A y 149B. San Francisco - Edo. Zulia. Venezuela<br> 
																			Teléfono: 58- 261-7362575<br> 
																			Correos electrónicos:<br> 
																			 j.tamayo@sistemasdepropulsion.com<br> 
																			 m.fuentes@sistemasdepropulsion.com </td> 
                              </tr> 
                          </table> 
                          <!--[if mso]> 
                          </td> 
                          </tr> 
                          </table> 
                          <![endif]--> 
                      </div> 
                  </td> 
              </tr> 
          </table> 
          <!-- Full Bleed Background Section : END --> 
       
          </center>\\n 
      </body>\\n 
      </html>`;
	return mailTransport.sendMail(mailOptions).then(() => {
		console.log('Nuevo usuaro registrado:', email);
	});
}

// Sends a goodbye email to the given user.
function sendGoodbyEmail(email) {
	const mailOptions = {
		from: `${APP_NAME} <noreply@firebase.com>`,
		to: email
	};

	// The user unsubscribed to the newsletter.
	mailOptions.subject = `Hasta Pronto ${email || ''}!`;
	mailOptions.text = `:( Confirmamos que se ha eliminado tu cuenta en ${APP_NAME} .`;
	return mailTransport.sendMail(mailOptions).then(() => {
		console.log('Usuario eliminado:', email);
	});
}
