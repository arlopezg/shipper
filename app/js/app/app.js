(function() {
  "use strict";
  /**
   * @ngdoc overview
   * @name shipperApp
   * @description
   * # shipperApp
   *
   * Main module of the application.
   */
  angular.module("shipperApp", [
      "ngAnimate",
      "ngCookies",
      "ngResource",
      "ui.router",
      "ui.router.state.events",
      "ngSanitize",
      "ngTouch",
      "ngStorage",
      "ngMessages",
      "firebase",
      "ngToast",
      "ngCart",
      "angularUtils.directives.dirPagination",
      "ui.bootstrap.datetimepicker",
      "pascalprecht.translate",
      /** Modules **/
      "shipper.site",
      "shipper.translate",
      "shipper.cart",
      "shipper.exceptions",
      "shipper.firebase",
      "shipper.routing",
      "shipper.security",
      "shipper.users",
      "shipper.tech",
      "shipper.products",
      "shipper.services",
      "shipper.reports",
    ]);
/*****************************************/
})();
