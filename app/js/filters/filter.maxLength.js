/**
 * @desc Limitar la longitud de un string
 * @example <ANY ng-bind="vm.xyz | maxLength: (integer)"></ANY>
 */
angular
.module('shipper.site')
.filter('maxLength', filter);

filter.$inject = [];

function filter() {
	return function(text, limit) {
		let changedString = String(text).replace(/<[^>]+>/gm, "");
		let length        = changedString.length;
		let suffix        = "...";
		
		return length > limit ? changedString.substr(0, limit-1)+suffix:changedString;
	}
}