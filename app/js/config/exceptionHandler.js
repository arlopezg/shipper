(function () {
	angular.module("shipper.exceptions", [])
  .config(function($provide) {
    $provide.decorator("$exceptionHandler", ['$delegate', 'CommonFactory', function ($delegate,CommonFactory) {
      return function (exception, cause) {
        $delegate(exception, cause);
        CommonFactory.displayMessage("Ha ocurrido un error", "danger");
        console.log({"[ExceptionHandler] Message":exception});
      };
    }]);
  })
})();
