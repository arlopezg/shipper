(function () {
  "use strict";
  
  angular.module("shipperApp")
  .run(function ($rootScope, FirebaseFactory, CommonFactory, UsersFactory, $state) {
    /** Variables **/
    let isLogged;
    let required;
    required = {"role": []};
    /***************/
    
    /*******Procesos**********/
    $rootScope.$on("$stateChangeStart", (event, toState) => {
      isLogged = UsersFactory.getAuthData();
      required.role = toState.data.requireLogin;
      if((!!required.role && !isLogged) || (!!required.role && !isAllowed(isLogged.role))) {
        CommonFactory.displayMessage("No tienes permisos suficientes", "danger");
        event.preventDefault();
        $state.go("site");
      }
    });
    /*************************/
    
    /****Funciones generales**/
    function isAllowed(role) {
      let pertenece = false;
      required.role.map((requiredRoles) => {
        if(requiredRoles === role)
          pertenece = true;
      });
      return pertenece;
    }
    /*************************/
  });
})();
