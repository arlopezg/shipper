(function () {
	angular.module("shipper.routing", [])
	.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
		let required = {
			"admin": ["admin"],
			"technician": ["admin", "technician"],
			"operator": ["admin", "operator"],
			"crew": ["admin", "operator", "tecnician"],
			"any": ["admin", "operator", "technician", "client"],
			"false": false,
			// difference between .any and .false is requiring to at least be logged, and not.
		};
		
		$locationProvider.hashPrefix("");
		$stateProvider
		.state("site", {
			name: "site",
			url: "/",
			data: {"requireLogin": required.false},
			views: {
				"": {templateUrl: "src/structure/structure.html"},
				"header@site": {
					templateUrl: "src/structure/header.html",
					controller: "SecurityController",
					controllerAs: "vm"
				},
				"content@site": {
					templateUrl: "src/dashboard/dashboard.html",
					controller: "DashboardController",
					controllerAs: "vm"
				},
				"footer@site": {templateUrl: "src/structure/footer.html"}
			}
		})
		
		.state("auth", {
			name: "auth",
			url: "/users/",
			data: {"requireLogin": required.false},
			views: {
				"": {templateUrl: "src/structure/structure.html"},
				"header@auth": {
					templateUrl: "src/structure/header.html",
					controller: "SecurityController",
					controllerAs: "vm"
				},
				"footer@auth": {templateUrl: "src/structure/footer.html"}
			},
		})
		.state("auth.login", {
			name: "auth.login",
			url: "login/?goBackTo",
			data: {"requireLogin": required.false},
			views: {
				"content@auth": {
					templateUrl: "src/users/login.html",
					controller:"SecurityController",
					controllerAs: "vm",
				}
			}
		})
		.state("auth.register", {
			name: "auth.register",
			url: "register/",
			views: {
				"content@auth": {
					templateUrl: "src/users/register.html",
					controller:"SecurityController",
					controllerAs: "vm",
				}
			}
		})
		.state("auth.resetPassword", {
			name: "auth.resetPassword",
			url: "resetPassword/",
			views: {
				"content@auth": {
					templateUrl: "src/users/resetPassword.html",
					controller:"SecurityController",
					controllerAs: "vm",
				}
			}
		})
		
		.state("site.contact", {
			name: "site.contact",
			url: "contact/",
			data: {"requireLogin": required.false},
			views: {
				"content@site": {
					templateUrl: "src/site/contact.html",
					controller: "ContactController",
					controllerAs: "vm"
				}
			},
		})
		
		.state("site.cart", {
			name: "site.cart",
			url: "cart/",
			data: {"requireLogin": required.false},
			views: {
				"content@site": {
					templateUrl: "src/cart/summary.html",
					controller: "CartController",
					controllerAs: "vm"
				}
			},
		})
		
		.state("site.me", {
			name: "site.me",
			url: "users/",
			data: {"requireLogin": required.any},
			views: {
				"content@site": {
					templateUrl: "src/users/myprofile.html",
					controller: "UsersController",
					controllerAs: "vm"
				}
			}
		})
		
		.state("site.products", {
			name: "site.products",
			url: "products/",
			data: {"requireLogin": required.false},
			views: {
				"content@site": {
					templateUrl: "src/products/products.list.html",
					controller: "ProductsController",
					controllerAs: "vm"
				}
			},
		})
		.state("site.products.details", {
			name: "site.products.details",
			url: ":productId",
			data: {"requireLogin": required.false},
			views: {
				"content@site": {
					templateUrl: "src/products/product.details.html",
					controller: "ProductDetailsController",
					controllerAs: "vm"
				}
			},
		})
		
		.state("site.services", {
			name: "site.services",
			url: "services/",
			data: {"requireLogin": required.false},
			views: {
				"content@site": {
					templateUrl: "src/services/services.list.html",
					controller: "ServicesController",
					controllerAs: "vm"
				}
			},
		})
		.state("site.services.details", {
			name: "site.services.details",
			url: ":serviceId",
			data: {"requireLogin": required.false},
			views: {
				"content@site": {
					templateUrl: "src/services/service.details.html",
					controller: "ServicesDetailsController",
					controllerAs: "vm"
				}
			},
		})
		
		.state("site.admin", {
			name: "site.admin",
			url: "admin/",
			data: {"requireLogin": required.crew},
			views: {
				"content@site": {
					templateUrl: "src/site/admin/menu.html",
					controller: "AdministrationController",
					controllerAs: "vm"
				}
			},
		})
		.state("site.admin.reports", {
			name: "site.admin.reports",
			url: "reports/",
			data: {"requireLogin": required.crew},
			views: {
				"adminContentCard@site.admin": {
					templateUrl: "src/site/admin/reports/reports.html",
					controller: "ReportsController",
					controllerAs: "vm",
				}
			},
		})
		.state("site.admin.config", {
			// configure settings as RAZOSOCI, RIF, DIRECCION, TLF, EMAIL...
			name: "site.admin.config",
			url: "config/",
			data: {"requireLogin": required.admin},
			views: {
				"adminContentCard@site.admin": {
					templateUrl: "",
					controller: "",
				}
			},
		})
		.state("site.admin.products", {
			name: "site.admin.products",
			url: "products/",
			data: {"requireLogin": required.operator},
			views: {
				"adminContentCard@site.admin": {
					templateUrl: "src/site/admin/products/products-management.html",
					controller: "ProductsController",
					controllerAs: "vm"
				}
			},
		})
		.state("site.admin.services", {
			name: "site.admin.services",
			url: "services/",
			data: {"requireLogin": required.operator},
			views: {
				"adminContentCard@site.admin": {
					templateUrl: "src/site/admin/services/services-management.html",
					controller: "ServicesController",
					controllerAs: "vm"
				}
			},
		})
		.state("site.admin.users", {
			name: "site.admin.users",
			url: "users/",
			data: {"requireLogin": required.operator},
			views: {
				"adminContentCard@site.admin": {
					templateUrl: "src/site/admin/users/users-management.html",
					controller: "UsersConfigController",
					controllerAs: "vm"
				}
			},
		})
		.state("site.admin.tech", {
			name: "site.admin.tech",
			url: "tech/",
			data: {"requireLogin": required.technician},
			views: {
				"adminContentCard@site.admin": {
					templateUrl: "src/site/admin/tech/tech.html",
					controller: "TechnicianController",
					controllerAs: "vm"
				}
			},
		})
		.state("site.admin.sales", {
			name: "site.admin.sales",
			url: "sales/",
			data: {"requireLogin": required.operator},
			views: {
				"adminContentCard@site.admin": {
					templateUrl: "src/site/admin/marketing/marketing.html",
					controller: "ContactController",
					controllerAs: "vm"
				}
			},
		})
		
		.state("otherwise", {
			url: "*path",
			data: {"requireLogin": required.false},
			templateUrl: "src/site/404.html"
		});
	});
})();
