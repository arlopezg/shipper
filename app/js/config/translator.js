// https://github.com/angular-translate/angular-translate/blob/master/demo/ex14_translate_language.htm#L85
(function() {
	angular.module("shipper.translate", [])
	.config(translate);
	
	translate.$inject = ["$translateProvider"];
	
	function translate($translateProvider) {
		// Adding a translation table for the English language
		$translateProvider.translations('en-US', {
			"ADD":                    "Add",
			"ALL_CATEGORIES":         "All categories",
			"ALL_RIGHTS_RESERVED":    "All rights reserved",
			"ARTICLE":                "article",
			"CONTACT":                "Contact",
			"ENGLISH":                "English",
			"FILTER":                 "Filter",
			"HOME":                   "Home",
			"LOGIN":                  "Sign in",
			"LOGOUT":                 "Sign out",
			"NO_ITEMS_AVAILABLE":     "we couldn't find any available items",
			"OR":                     "or",
			"AND":                    "and",
			"NOTHING_IN_HERE":        "There's nothing in here!",
			"YOU_MAY_BE_LOOKING_FOR": "You may be looking for",
			"ORDER_BY":               "Order by",
			"OUR_PRODUCTS":           "Our products",
			"OUR_SERVICES":           "Our services",
			"PRODUCTS":               "Products",
			"READ_IN":                "Read in",
			"READ_MORE":              "read more",
			"REMOVE":                 "Remove",
			"SELECT_LANGUAGE":        "Select a language...",
			"SEND":                   "Send",
			"EMAIL_ADDRESS":          "Email address",
			"PASSWORD":               "Password",
			"SERVICES":               "Services",
			"SPANISH":                "Spanish",
			"STOCK":                  "Stock",
			"SUBSCRIBE":              "Do you want to receive information about new products, services and discounts?",
			"VIEW_ALL":               "View all",
			"WE_ARE_SORRY":           "We're sorry",
			"SIGNUP":                 "Sign up",
			"FORGOT_PASSWORD":        "I forgot my password!",
			"HEADER":                 "You can translate texts by using a filter.",
			"SUBHEADER":              "And if you don't like filters, you can use a directive.",
			"HTML_KEYS":              "If you don't like an empty elements, you can write a key for the translation as an inner HTML of the directive.",
			"DATA_TO_FILTER":         "Your translations might also contain any static ({{staticValue}}) or random ({{randomValue}}) values, which are taken directly from the model.",
			"DATA_TO_DIRECTIVE":      "And it's no matter if you use filter or directive: static is still {{staticValue}} and random is still {{randomValue}}.",
			"RAW_TO_FILTER":          "In case you want to pass a {{type}} data to the filter, you have only to pass it as a filter parameter.",
			"RAW_TO_DIRECTIVE":       "This trick also works for {{type}} with a small mods.",
			"SERVICE":                "Of course, you can translate your strings directly in the js code by using a $translate service.",
			"SERVICE_PARAMS":         "And you are still able to pass params to the texts. Static = {{staticValue}}, random = {{randomValue}}."
		});
		
		// Adding a translation table for the Russian language
		$translateProvider.translations('es-ES', {
			"ADD":                    "Añadir",
			"ALL_CATEGORIES":         "Todas las categorías",
			"ALL_RIGHTS_RESERVED":    "Todos los derechos reservados",
			"ARTICLE":                "artículo",
			"CONTACT":                "Contacto",
			"ENGLISH":                "Inglés",
			"FILTER":                 "Filtrar",
			"HOME":                   "Inicio",
			"LOGIN":                  "Conéctate",
			"LOGOUT":                 "Cerrar sesión",
			"NO_ITEMS_AVAILABLE":     "No se encontraron artículos disponibles",
			"OR":                     "o",
			"AND":                    "y",
			"ORDER_BY":               "Ordenar por",
			"SIGNUP":                 "Regístrate",
			"FORGOT_PASSWORD":        "¡Olvidé mi contraseña!",
			"EMAIL_ADDRESS":          "Correo electrónico",
			"PASSWORD":               "Contraseña",
			"OUR_PRODUCTS":           "Nuestros productos",
			"OUR_SERVICES":           "Nuestros servicios",
			"NOTHING_IN_HERE":        "¡No has añadido nada al carrito!",
			"YOU_MAY_BE_LOOKING_FOR": "Tal vez estés estés interesado en",
			"PRODUCTS":               "Productos",
			"READ_IN":                "Leer en",
			"READ_MORE":              "leer más",
			"REMOVE":                 "Remover",
			"SELECT_LANGUAGE":        "Selecciona un lenguaje...",
			"SEND":                   "Enviar",
			"SERVICES":               "Servicios",
			"SPANISH":                "Español",
			"STOCK":                  "Existencia",
			"SUBSCRIBE":              "Suscríbete para recibir información de nuevos productos, servicios y ofertas",
			"VIEW_ALL":               "Ver todo",
			"WE_ARE_SORRY":           "Lo sentimos"
		});
		
		// Tell the module what language to use by default
		$translateProvider.useSanitizeValueStrategy('escapeParameters');
		$translateProvider.preferredLanguage('en-US');
		
	}
})();
