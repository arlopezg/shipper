/**
 * @desc Lista de los diferentes productos
 * @example <loader arr="[]"></loader>
 */
angular
.module('shipper.site')
.directive('loader', directive);

directive.$inject = [];

function directive() {
	let vm = this;
	let directive;
	
	directive = {
		restrict: "E",
		scope: {"arr": "="},
		templateUrl: "js/directives/loader/loader-directive.html"
	};
	return directive;
}