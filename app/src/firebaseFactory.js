(function (fireAuth) {
	"use strict";
	angular
	.module("shipper.firebase", [])
	.factory("FirebaseFactory", factory);
	
	factory.$inject = ["$firebaseObject", "$firebaseArray", "$firebaseAuth", "firebase", "CommonFactory"];
	
	function factory($firebaseObject, $firebaseArray, $firebaseAuth, firebase, CommonFactory) {
		/*********** Variables *******************/
		/*********** Fin de Variables ************/
		
		/*********** Menu de funciones ***********/
		/*********** Fin menu de funciones********/
		
		/*********** Procesos ********************/
		return {
			getRef_usuarios: getRef_usuarios,
			getRef_categories: getRef_categories,
			getRef_clientes: getRef_clientes,
			getRef_productos: getRef_productos,
			getRef_servicios: getRef_servicios,
			getRef_contacto: getRef_contacto,
			getRef_newsletter: getRef_newsletter,
			getRef_audit: getRef_audit,
			getCurrentAuth: getCurrentAuth,
			resetPassword: resetPassword,
		};
		/*********** Fin de process **************/
		
		/***********Funciones especificas ********/
		function getRef_audit() {
			return firebase.database().ref("audit");
		}
		
		function getRef_usuarios() {
			return firebase.database().ref("usuarios");
		}
		
		function getRef_clientes() {
			return firebase.database().ref("clientes");
		}
		
		function getRef_productos() {
			return firebase.database().ref("productos");
		}
		
		function getRef_contacto() {
			return firebase.database().ref("contacto");
		}
		
		function getRef_newsletter() {
			return firebase.database().ref("newsletter");
		}
		
		function getRef_categories() {
			return firebase.database().ref("config/categories");
		}
		
		function getRef_servicios() {
			return firebase.database().ref("servicios");
		}
		
		function getCurrentAuth() {
			return firebase.auth().currentUser;
		}
		
		function resetPassword(email, callback) {
			firebase.auth().sendPasswordResetEmail(email)
			.then((data) => {
				if(callback)
					callback(true);
				CommonFactory.displayMessage("Solicitud de cambio de contraseña enviada", "success");
			})
			.catch((error) => {
				if(callback)
					callback(false, error);
				CommonFactory.displayMessage("Ha ocurrido un error", "warning");
			});
		}
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		/*********** Fin de funciones generales ***/
	}
})(firebase.auth());
