(function () {
  'use strict';
  angular
  .module('shipper.products')
  .controller('DashboardController', controller);
  
  controller.$inject = ['ServicesFactory', 'ProductsFactory', 'CommonFactory', "ContactFactory"];
  
  function controller(ServicesFactory, ProductsFactory, CommonFactory, ContactFactory) {
    /*********** Variables *******************/
    let vm = this;
    /*****************************************/
    
    /*********** Menu de funciones ***********/
    vm.regexes = CommonFactory.regexes();
	  vm.addToNewsletter = addToNewsletter;
	  /*****************************************/
    
    /*********** Procesos ********************/
    getServices();
    getProducts();
    /*****************************************/
    
    /***********Funciones especificas ********/
    function addToNewsletter(email) {
	    console.log(email);
	    ContactFactory.addToNewsletter(email, result);
	    function result(data) {
		    console.log(data);
		    if(!!data)
			    vm.lockNewsletter = true;
	    }
    }
	
	  /*****************************************/
  
    /*********** Funciones Generales **********/
    function getServices() {
	    vm.services = ServicesFactory.getAll();
    }
    
    function getProducts() {
	    vm.products = ProductsFactory.getAll();
    }
    /*****************************************/
  }
})();
