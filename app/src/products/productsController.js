(function () {
	'use strict';
	
	angular
	.module('shipper.products')
	.controller('ProductsController', controller);
	
	controller.$inject = ['ProductsFactory', 'CommonFactory', 'CartFactory'];
	
	function controller(ProductsFactory, CommonFactory, CartFactory) {
		/*********** Variables *******************/
		let vm = this;
		vm.product = {};
		vm.regexes = CommonFactory.regexes();
		vm.disableProductModal = false;
		/*****************************************/
		
		/*********** Menu de funciones ***********/
		vm.addProduct = addProduct;
		vm.addCategory = addCategory;
		vm.changeProductStatus = changeProductStatus;
		vm.getProductData = getProductData;
		vm.setProductData = setProductData;
		vm.quickAddToCart = quickAddToCart;
		vm.quickRemoveFromCart = quickRemoveFromCart;
		vm.manageCategories = manageCategories;
		vm.thousandSeparator = CommonFactory.thousandSeparator;
		vm.itemInCart = CartFactory.getItemById;
		/*****************************************/
		
		/*********** Procesos ********************/
		CommonFactory.initTooltip();
		getAll();
		getCategories();
		/*****************************************/
		
		/***********Funciones especificas ********/
		function addProduct(product) {
			ProductsFactory.addProduct(product);
		}
		
		function quickAddToCart(product, quantity) {
			CartFactory.addItem(product.id, product.name, product.price, quantity, product, "product");
		}
		
		function quickRemoveFromCart(id) {
			CartFactory.removeItem(id);
		}
		
		function changeProductStatus(id) {
			ProductsFactory.changeProductStatus(id);
		}
		
		function getProductData(product, disable) {
			// So it doesn't get updated on the go
			if(product)
				vm.product = angular.copy(product);
			// disable product form
			vm.disableProductModal = !!disable;
		}
		
		function setProductData(product) {
			if(product.id)
				ProductsFactory.updateProduct(product.id, product);
			else
				ProductsFactory.addProduct(product);
		}
		
		function addCategory(category) {
			ProductsFactory.addCategory(category, result);
			function result(response) {
				if(response === true)
					vm.category = {};
			}
		}

		function manageCategories(action, data) {
			ProductsFactory.manageCategories(action, data);
		}
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		function callback(data) {
			console.log("[ProductController]",data);
		}
		
		function getAll() {
			vm.products = ProductsFactory.getAll();
		}
		
		function getCategories() {
			vm.categories = ProductsFactory.getCategories();
		}
		/*********** Fin de funciones generales ***/
	}
})();
