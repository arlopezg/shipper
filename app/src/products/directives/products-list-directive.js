/**
 * @desc Lista de los diferentes productos
 * @example <products-list products="" name="" category="" order="" limit="" count=""></products-list>
 */
angular
.module('shipper.products')
.directive('productsList', directive);

directive.$inject = [];

function directive() {
	let vm = this;
	let directive;
	
	directive = {
		restrict: "E",
		scope: {
			"products": "=",
			"name": "=",
			"category": "=",
			"order": "=",
			"itemsPerPage": "=",
			"limit": "="
		},
		templateUrl: "src/products/directives/products-list-directive.html",
		controller: "ProductsController",
		controllerAs: "vm"
	};
	
	return directive;
}