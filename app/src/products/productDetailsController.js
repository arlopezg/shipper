(function () {
  'use strict';
  
  angular
  .module('shipper.products')
  .controller('ProductDetailsController', controller);
  
  controller.$inject = ['ProductsFactory', 'CommonFactory', '$state', 'CartFactory'];
  
  function controller(ProductsFactory, CommonFactory, $state, CartFactory) {
    /*********** Variables *******************/
    let vm = this;
    let CODIPROD = $state.params.productId;
    vm.regexes = CommonFactory.regexes();
    vm.disableProductModal = false;
    /*****************************************/
    
    /*********** Menu de funciones ***********/
    vm.thousandSeparator = CommonFactory.thousandSeparator;
    vm.addToCart = addToCart;
    vm.removeItem = removeItem;
    vm.itemInCart = CartFactory.getItemById;
    /*****************************************/
    
    /*********** Procesos ********************/
    CommonFactory.initTooltip();
    getProductData();
    /*****************************************/
    
    /***********Funciones especificas ********/
    function addToCart(product, quantity) {
      CartFactory.addItem(product.id, product.name, product.price, quantity, product, "product");
    }
    
    function removeItem(id) {
      CartFactory.removeItem(id);
    }
    /*********** Fin Funciones especificas ***/
    
    /*********** Funciones Generales **********/
    function callback(data) {
      console.log(data);
    }
    
    function getProductData() {
      vm.product = ProductsFactory.getProductById(CODIPROD);
      if(CartFactory.getItemById(CODIPROD))
      	vm.product.addToCartQuantity = CartFactory.getItemById(CODIPROD)._quantity;
    }
    /*********** Fin de funciones generales ***/
  }
})();
