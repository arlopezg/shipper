(function () {
  'use strict';
  angular
  .module('shipper.products', [])
  .factory('ProductsFactory', factory);
  
  factory.$inject = ['FirebaseFactory', '$firebaseObject', '$firebaseArray', 'CommonFactory',
    '$localStorage', 'AuditFactory'];
  
  function factory(FirebaseFactory, $firebaseObject, $firebaseArray, CommonFactory,
                   $localStorage, AuditFactory) {
    /*********** Variables *******************/
    const ref_productos = FirebaseFactory.getRef_productos();
    const ref_categories = FirebaseFactory.getRef_categories().child("products");
    let push;
    let producto;
    /*********** Fin de Variables ************/
    
    /*********** Menu de funciones ***********/
    /*********** Fin menu de funciones********/
    
    /*********** Procesos ********************/
    return {
	    getAll: getAll,
	    getProductById: getProductById,
	    getCategories: getCategories,
	    addProduct: addProduct,
	    updateProduct: updateProduct,
	    manageCategories: manageCategories,
	    addToCart: addToCart,
	    changeProductStatus: changeProductStatus,
    };
    /*********** Fin de process **************/
    
    /***********Funciones especificas ********/
    function getAll() {
	    return $firebaseArray(ref_productos);
    }
	
	  function getProductById(uid) {
		  return $firebaseObject(ref_productos.child(uid));
	  }
	
	  function getCategories() {
		  return $firebaseArray(ref_categories);
	  }
	
	  function addProduct(product) {
		  push = ref_productos.push();
		  product.id = push.key;
		  product.enabled = true;
		  product.registerDate = new Date().getTime();
		  ref_productos.child(`/${push.key}`).set(product)
		  .then(() => {
			  CommonFactory.displayMessage("Producto creado con éxito", "success");
			  setAudit(`created product ${product.id}`);
		  })
		  .catch(() => {CommonFactory.displayMessage("Error al crear producto", "danger")});
	  }
	
	  function changeProductStatus(id, callback) {
		  producto = getProductById(id);
		  producto.$loaded().then(() => {
			  ref_productos.child(producto.id).update({"enabled": !producto.enabled})
			  .then(() => {
				  if(callback)
					  callback(true);
				  CommonFactory.displayMessage("Producto actualizado", "success");
				  setAudit(`changed product '${id}' status to '${!producto.enabled}'`);
			  })
			  .catch(() => {CommonFactory.displayMessage("Error al modificar producto", "danger")});
		  });
	  }
	
	  function updateProduct(id, product) {
		  delete product.$id;
		  delete product.$priority;
		  producto = getProductById(id);
		  producto.$loaded().then(() => {
			  ref_productos.child(producto.id).update(product)
			  .then(() => {
			  	CommonFactory.displayMessage("Producto actualizado", "success");
					setAudit(`updated product '${id}' data`);
			  })
			  .catch(() => {CommonFactory.displayMessage("Error al modificar producto", "danger")});
		  });
	  }
	
	  function addToCart(id, price, quantity) {
		  let cart = $localStorage.cart;
		  if(CommonFactory.findIndexByKeyValue(cart, "id", id)) {
			  let index = cart.findIndex(item => item.id === id);
			  cart.splice(index, 1);
		  } else
			  cart.push({"id": id, "price": price, "quantity": quantity});
	  }
	
	  function manageCategories(action, category, callback) {
	  	switch(true) {
	  		case action === "add":
	  		push = ref_categories.push();
			  category.id = push.key;
			  ref_categories.child(`/${category.id}`).set(category)
			  .then(() => {
				  CommonFactory.displayMessage("Categoría creada con éxito", "success");
				  setAudit(`created category '${category.name}'`);
			  })
			  .catch(() => {CommonFactory.displayMessage("Error al crear categoría", "danger")});
	  		break;
	  		case action === "delete":
			  ref_categories.child(`/${category.id}`).remove()
			  .then(() => {
				  CommonFactory.displayMessage("Categoría eliminada con éxito", "success");
				  setAudit(`deleted category '${category.name}'`);
			  })
			  .catch(() => {CommonFactory.displayMessage("Error al eliminar categoría", "danger")});
	  		break;
	  	}
	  }
    /*********** Fin Funciones especificas ***/
    
    /*********** Funciones Generales **********/
    function setAudit(action) {
	    AuditFactory.setAudit(action);
    }
    /***********Fin de funciones generales*****/
  }
})();
