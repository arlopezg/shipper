/**
 * Created by EddieM on 09/05/2017.
 */
(function () {
	'use strict';
	
	angular
	.module('shipperApp')
	.factory('CommonFactory', factory);
	
	factory.$inject = ['$filter', 'ngToast', "$state"];
	
	function factory($filter, ngToast, $state) {
		/*********** Variables *******************/
		/*********** Fin de Variables ************/
		
		/*********** Procesos ********************/
		return {
			regexes: regexes,
			identityDocumentsInfo: identityDocumentsInfo,
			userRoles: userRoles,
			initClockpicker: initClockpicker,
			initTooltip: initTooltip,
			preventCollapseOnDesktop: preventCollapseOnDesktop,
			findIndexByKeyValue: findIndexByKeyValue,
			phonePreffixesList: phonePreffixesList,
			displayMessage: displayMessage,
			thousandSeparator: thousandSeparator,
			setPage: setPage,
			findObjectInArray: findObjectInArray,
			findIndexInArray: findIndexInArray,
			sortArrayByProp: sortArrayByProp,
			timestampToDate: timestampToDate,
		};
		/*********** Fin de process **************/
		
		
		/***********Funciones especificas ********/
		function timestampToDate(timestamp) {
			return $filter('date')(timestamp, 'dd/MM/yyyy hh:mm a');
		}
		
		function preventCollapseOnDesktop() {
			$('[data-toggle="collapse"]').click((event) => {
				if (window.innerWidth >= 768)
					event.stopPropagation();
			})
		}
		
		function displayMessage(message, className, cause, linkTo) {
			ngToast.create({"className": className, "content": message});
			if(cause)
				console.log("[Dev] Cause:", cause);
			// if(linkTo)
			// ...redirect to linkTo (state?)
		}
		
		function regexes() {
			return {
				"phone_number": /((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/,
				"email": /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
				"date": /^([0][1-9]|[12][0-9]|3[01])(-)([0][1-9]|[1][0-2])\2(\d{4})$/, //ToDo! allow "...XX:XX am/pm"
				"name": /^[a-zA-Zá-źÁ-Ź0-9\/\-,.'"¿?¡! ]{1,30}$/,
				"description": /^[a-zA-Zá-źÁ-Ź0-9\/\-,.'"¡!¿?() \n]{1,3000}$/,
				"username": /^[a-zA-Z0-9\/_. ]{4,30}$/,
				"alphanumeric_255": /^[a-zA-Zá-źÁ-Ź0-9\-\/,.'()¿?¡! ]{1,255}$/,
				"price": /^\d{1,252}([.|,]\d{1,2})?$/,
				"numeric_255": /^[0-9]{1,255}$/,
				"numeric_9": /^[0-9]{6,9}$/,
				"time_military": /^([01]\d|2[0-3]):([0-5]\d)$/,
				"password": /^(?=.*\d).{6,30}$/,
				"hexcolor": /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/
			};
		}
		
		function phonePreffixesList() {
			return [
				{"carrier":"Digitel", "number":"0412"},
				{"carrier":"Movistar", "number":"0414"},
				{"carrier":"Movistar", "number":"0424"},
				{"carrier":"Movilnet", "number":"0416"},
				{"carrier":"Movilnet", "number":"0426"},
			];
		}
		
		function identityDocumentsInfo() {
			return [
				{"type": "Natural", "value":"V"},
				{"type": "Jurídico", "value":"J"},
				{"type": "Extranjero", "value":"E"},
			];
		}
		
		function userRoles() {
			return [
				{"name": "Administrador", "value": "admin"},
				{"name": "Operador", "value": "operator"},
				{"name": "Técnico", "value": "technician"},
				{"name": "Cliente", "value": "client"},
			];
		}

		function initClockpicker() {
			$('.clockpicker').clockpicker({
				twelvehour: false,
				placement: "right",
				align: "top",
				autoclose: "true"
			});
		}
		
		function initTooltip() {
			$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();
			});
		}
		
		function findIndexByKeyValue(arr, key_name, val) {
			let obj;
			if(arr)
				obj = arr.find(item => item[key_name] === val);
			return obj;
		}
		
		function thousandSeparator(numb) {
			if(!numb)
				numb = "";
			else if(typeof numb === "string")
				numb = parseFloat(numb).toLocaleString('de-DE');
			else
				numb = numb.toLocaleString('de-De');
			return numb;
		}
		
		function findObjectInArray(arr, key_name, val) {
			let obj;
			if(arr)
				obj = arr.find(item => item[key_name] === val);
			console.log("Array:", arr);
			console.log("Key:", key_name);
			console.log("Value:", val);
			console.log("Found:", obj);
			return obj;
		}
		
		function findIndexInArray(arr, key_name, val) {
			let pos = -1;
			arr.map((item, index) => {
				if(item[key_name] === val)
					pos = index;
			});
			return pos;
		}
		
		function setPage(arr, page, count) {
			let viewable = [];
			
			for(let i=(page-1)*count; i<page*count; i++)
				viewable.push(arr[i]);
			
			return viewable;
		}
		
		function sortArrayByProp(arr, prop){
			/**
			 * Sorts an array of objects "in place".
			 * (Meaning that the original array will be modified and nothing gets returned.)
			 * **/
			arr.sort(
				function (a, b) {
					if (a[prop] < b[prop]){
						return -1;
					} else if (a[prop] > b[prop]){
						return 1;
					} else {
						return 0;
					}
				}
			);
		}
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		/*********** Fin de funciones generales ***/
	}
})();
