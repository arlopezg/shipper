/**
 * @desc Lista de los diferentes productos
 * @example <request-status switch="data"></request-status>
 */
angular
.module('shipper.site')
.directive('requestStatus', directive);

directive.$inject = [];

function directive() {
	let vm = this;
	let directive;
	
	directive = {
		restrict: "E",
		scope: {
			"switch": "=",
		},
		templateUrl: "src/site/admin/marketing/directives/request-status-directive.html",
		controller: "ContactController",
		controllerAs: "vm"
	};
	
	return directive;
}