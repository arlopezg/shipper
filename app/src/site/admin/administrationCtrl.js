(function () {
	'use strict';
	
	angular
	.module('shipper.site')
	.controller('AdministrationController', controller);
	
	controller.$inject = ["ContactFactory", "CommonFactory", "$timeout"];
	
	function controller(ContactFactory, CommonFactory, $timeout) {
		/*********** Variables *******************/
		let vm = this;
		/*****************************************/
		
		/*********** Menu de funciones ***********/
		/*****************************************/
		
		/*********** Procesos ********************/
		vm.contactCount = ContactFactory.getAll();
		/*****************************************/
		
		/***********Funciones especificas ********/
		/****************************************/
		
		/*********** Funciones Generales ********/
		/****************************************/
	}
})();
