(function() {
	'use strict';
	
	angular
	.module('shipper.tech', [])
	.controller('TechnicianController', controller);
	
	controller.$inject = ['ContactFactory', 'DatepickerFactory', '$timeout', 'UsersFactory'];
	
	function controller(ContactFactory, DatepickerFactory, $timeout, UsersFactory) {
		/*********** Variables *******************/
		let vm = this;
		vm.options = DatepickerFactory.setOptions();
		/*****************************************/
		
		/*********** Menu de funciones ***********/
		vm.reply                 = reply;
		vm.getMessageData        = getMessageData;
		vm.startDateBeforeRender = DatepickerFactory.setMinDayToday;
		vm.cleanUpData           = cleanUpData;
		/*****************************************/
		
		/*********** Procesos ********************/
		getMessages();
		/*****************************************/
		
		/***********Funciones especificas ********/
		function reply(msgId, reply) {
			let status = (!!reply.status) ? "Planificado":"Rechazado";
			delete reply.status;
			console.log(status, reply);
			ContactFactory.replyToMessage(msgId, reply, status, result);
			function result(data) {
				if(data) {
					vm.message = {};
					$("#messageModal").modal("toggle");
				}
			}
		}
		
		function cleanUpData() {
			vm.message.visit.date = "";
			vm.message.visit.comment = "";
		}
		
		function getMessageData(message, disable) {
			vm.message = angular.copy(message);
			if(!!message.data)
				vm.message.has = {
					"services": message.data.some(item => item._data.type === "service"),
					"products": message.data.some(item => item._data.type === "product")
				};
			else
				vm.message.has = false;
		}
		/*****************************************/
		
		/*********** Funciones Generales **********/
		function getMessages() {
			let logged = UsersFactory.getAuthData();
			$timeout(function() {
				if(logged.role === "admin")
					vm.messages = ContactFactory.getAll();
				else
					vm.messages = ContactFactory.getRequestsByTech(logged.$id);
				console.log("[TechCtrl] GetAll:", vm.messages);
			}, 4000);
		}
		/*****************************************/
	}
})();