(function () {
  'use strict';
  
  angular
  .module('shipper.users')
  .controller('UsersConfigController', controller);
  
  controller.$inject = ["UsersFactory", "CommonFactory", "ReportsFactory"];
  
  function controller(UsersFactory, CommonFactory, ReportsFactory) {
    /*********** Variables *******************/
    let vm = this;
    vm.users = [];
    vm.getUserData = getUserData;
    vm.timeSince = timeSince;
    vm.roles = CommonFactory.userRoles();
    /*****************************************/
    
    /*********** Menu de funciones ***********/
    vm.saveUser = saveUser;
    vm.print = print;
    /*****************************************/
    
    /*********** Procesos ********************/
    getUsers();
    CommonFactory.initTooltip();
    /*****************************************/
    
    /***********Funciones especificas ********/
    function getUserData(user) {
      vm.user = UsersFactory.getUserById(user.uid);
    }

    function timeSince(time) {
      return Number(moment().diff(time, "years"));
    }

    function print() {
      ReportsFactory.usersReport(vm.users);
    }

    function saveUser(user) {
      UsersFactory.updateUser(user.uid, user, result);
      function result(data) {
        if(data)
          vm.user = {};
      }
    }
    /*****************************************/
    
    /*********** Funciones Generales **********/
    function getUsers() {
      vm.users = UsersFactory.getAll();
    }
    /*****************************************/
  }
})();
