(function () {
	'use strict';
	angular
	.module("shipper.reports", [])
	.factory("ReportsFactory", factory);
	
	factory.$inject = ['LogoShipper','LogoTP','LogoGM','LogoSDP', 'CommonFactory', '$filter'];
	
	function factory(LogoShipper, LogoTP, LogoGM, LogoSDP, CommonFactory, $filter) {
		/*********** Variables *******************/
		let doc = new jsPDF();
		let width = doc.internal.pageSize.width;
		let height = doc.internal.pageSize.height;
		let decimals = CommonFactory.thousandSeparator;
		let empresas = {
			"SDP": {
				"ID": "J"+"-"+"307763639",
				"LOGO": LogoSDP,
				"NAME": "Sistemas de Propulsión, C.A.",
				"LOCATION": "Zona Industrial II etapa, PL 14, Av. 67A, entre calles 149B y 150, Municipio San Francisco, Maracaibo, Edo. Zulia",
				"CONTACT": {
					"EMAIL1": "j.tamayo@sistemasdepropulsion.com",
					"EMAIL2": "m.fuentes@sistemasdepropulsion.com",
					"TLF1": "0261-7360746",
					"TLF2": "0261-7360746",
				}
			},
			"TP": {
				"ID": "J"+"-"+"307763639",
				"LOGO": LogoTP,
				"NAME": "Tecnopotencia, C.A.",
				"LOCATION": "Zona Industrial Sector Los Robles, Av. 59 Nº 113A-131",
				"CONTACT": {
					"EMAIL1": "jgonzalez@tecnopotenciaonline.com",
					"EMAIL2": "mabadias@tecnopotenciaonline.com",
					"TLF1": "+58 (261) 7356045",
					"TLF2": "+58 (261) 7389143",
				}
			},
			"GM": {
				"ID": "J"+"-"+"307763639",
				"LOGO": LogoGM,
				"NAME": "Global Marine & Diesel Technology, C.A.",
				"LOCATION": "Av. 17, Local 120-310, Sector Los Haticos, Maracaibo, Zulia",
				"CONTACT": {
					"EMAIL1": "atencion@globalmarinedt.com",
					"EMAIL2": "mercadeo@globalmarinedt.com",
					"TLF1": "(0261) 7645421",
					"TLF2": "(414) 6190551",
				}
			},
		};
		/******************************************/
		
		/*********** Menu de funciones ***********/
		/******************************************/
		
		/*********** Procesos ********************/
		return {
			cartReport: cartReport,
			usersReport: usersReport,
			productsReport: productsReport,
			servicesReport: servicesReport,
			marketingReportDetailed: marketingReportDetailed,
			// techniciansReport: techniciansReport,
		};
		/******************************************/
		
		/***********Funciones especificas ********/
		function cartReport(items, REPOCODE, calculos, user) {
			let data = [];
			let options = getOptions();
			options["addPageContent"] = ((data) => {setFooter(data, REPOCODE)});
			let headers = [
				{"title": "Nombre", "dataKey": "item_name"},
				{"title": "Cantidad", "dataKey": "item_quantity"},
				{"title": "Precio unitario (Bs.)", "dataKey": "item_price"},
				{"title": "Subtotal (Bs.)", "dataKey": "item_subtotal"},
			];
			items.map((item) => {
				data.push({
					"item_name": item._name,
					"item_quantity": decimals(item._quantity),
					"item_price": decimals(item._price),
					"item_subtotal": decimals(item._price*item._quantity),
				});
			});
			doc = setHeaders(new jsPDF(), REPOCODE);
			doc.setTextColor(6, 6, 6);
			doc.setFontSize(14);
			doc.text("Solicitud de productos y/o servicios — Copia del cliente", 60, 45);
			doc.setFontSize(12);
			centerText("Productos y/o servicios solicitados:", 65);
			doc.autoTable(headers, data, options);
			doc.setFontSize(10);
			doc.text(`Subtotal: Bs. ${decimals(calculos.subtotal)}`, 25, doc.autoTableEndPosY()+15);
			doc.text(`I.V.A.: ${$filter("number")(calculos.IVA*100, 2)}%`, 25, doc.autoTableEndPosY()+20);
			doc.text(`Total: Bs. ${decimals(calculos.total)}`, 25, doc.autoTableEndPosY()+25);
			printUserData(user, doc.autoTableEndPosY()+15);
			doc.setFontSize(7);
			centerText(`Los precios mostrados y la disponibilidad de los productos/servicios se encuentran sujetos a cambio sin previo aviso`, height-5);
			doc.save(`Solicitud de Presupuesto_${REPOCODE}.pdf`);
		}
		
		function usersReport(users) {
			let data = [];
			let options = getOptions();
			options["addPageContent"] = ((data) => {setFooter(data)});
			users.map((user) => {
				data.push({
					"user_name": `${user.name} ${user.lastname}`,
					"user_role": user.role,
					"user_email": user.email,
					"user_phone": `${user.phone.preffix}-${user.phone.number}`,
				});
			});
			let headers = [
				{"title": "Nombre", "dataKey": "user_name"},
				{"title": "Rol", "dataKey": "user_role"},
				{"title": "Correo electrónico", "dataKey": "user_email"},
				{"title": "Número de teléfono", "dataKey": "user_phone"},
			];
			doc = setHeaders(new jsPDF());
			doc.setTextColor(6, 6, 6);
			doc.setFontSize(14);
			doc.text(`Reporte General de Usuarios — Copia del Operador`, 50, 45);
			doc.setFontSize(12);
			doc.text(`Total de Usuarios Registrados: ${data.length}`, width/3, 65);
			doc.autoTable(headers, data, options);
			doc.save("Reporte general de Usuarios.pdf");
		}
		
		function productsReport(products) {
			let data = [];
			let options = getOptions();
			options["addPageContent"] = ((data) => {setFooter(data)});
			products.map((product) => {
				data.push({
					"id": `${product.id}`,
					"product_name": `${product.name}`,
					"stock_type": product.stock.type,
					"stock_quantity": product.stock.quantity,
					"price": decimals(product.price),
				});
			});
			let headers = [
				{"title": "Nombre", "dataKey": "product_name"},
				{"title": "Precio", "dataKey": "price"},
				{"title": "Cantidad", "dataKey": "stock_quantity"},
				{"title": "Presentación", "dataKey": "stock_type"},
			];
			doc = setHeaders(new jsPDF());
			doc.setTextColor(6, 6, 6);
			doc.setFontSize(14);
			doc.text(`Reporte General de Productos — Copia del Operador`, 50, 45);
			doc.setFontSize(12);
			centerText(`Total de Productos Registrados: ${data.length}`, 65);
			doc.autoTable(headers, data, options);
			doc.setFontSize(7);
			centerText(`Los precios mostrados y la disponibilidad de los productos/servicios se encuentran sujetos a cambio sin previo aviso`, height-5);
			doc.save("Reporte general de Productos.pdf");
		}
		
		function servicesReport(services) {
			let options = getOptions();
			options["addPageContent"] = ((data) => {setFooter(data)});
			let data = [];
			services.map((service) => {
				data.push({
					"name": `${service.name}`,
					"description": service.description,
					"price": decimals(service.price),
				});
			});
			let headers = [
				{"title": "Nombre", "dataKey": "name"},
				{"title": "Descripción", "dataKey": "description"},
				{"title": "Precio (Bs).", "dataKey": "price"},
			];
			doc = setHeaders(new jsPDF());
			doc.setTextColor(6, 6, 6);
			doc.setFontSize(14);
			doc.text(`Reporte General de Servicios — Copia del Operador`, 50, 45);
			doc.setFontSize(12);
			doc.text(`Total de Servicios Registrados: ${data.length}`, width/3, 65);
			doc.autoTable(headers, data, options);
			doc.save("Reporte general de Servicios.pdf");
		}
		
		function marketingReportDetailed(requests) {
			let options = getOptions();
			options["addPageContent"] = ((data) => {setFooter(data)});
			let headers = [
				{"title": "Nombre", "dataKey": "item_name"},
				{"title": "Cantidad", "dataKey": "item_quantity"},
				{"title": "Precio unitario (Bs.)", "dataKey": "item_price"},
				{"title": "Subtotal (Bs.)", "dataKey": "item_subtotal"},
			];
			let data = [];
			requests.map((req) => {
				if(req.data)
					data.push(req);
			});
			doc = setHeaders(new jsPDF());
			doc.setTextColor(6, 6, 6);
			doc.setFontSize(14);
			doc.text(`Reporte Detallado de Comercialización`, 50, 45);
			doc.setFontSize(12);
			doc.text(`Total de Solicitudes Realizadas: ${data.length}`, width/3, 65);
			data.map((req) => {
				doc.addPage();
				let items = [];
				doc.setTextColor(6, 6, 6);
				doc.setFontSize(14);
				doc.text(`Código de solicitud: ${req.sentDate}`, 25, 65);
				req.data.map((item) => {
					doc = setHeaders(doc);
					doc.setTextColor(6, 6, 6);
					doc.setFontSize(14);
					doc.text(`Reporte Detallado de Comercialización`, 50, 45);
					items.push({
						"item_name": item._name,
						"item_quantity": decimals(item._quantity),
						"item_price": decimals(item._price),
						"item_subtotal": decimals(item._price*item._quantity)
					});
				});
				doc.autoTable(headers, items, options);doc.setFontSize(10);
				if(!!req.status)
					doc.text(`Estado: ${req.status.toUpperCase()}`, 25, doc.autoTableEndPosY()+10);
				else
					doc.text(`Estado: PENDIENTE`, 25, doc.autoTableEndPosY()+10);
				doc.text(`Total (con IVA): Bs. ${decimals(req.amount)}`, 25, doc.autoTableEndPosY()+15);
			});
			doc.save("Reporte Detallado de Comercialización.pdf");
		}
		/******************************************/
		
		/*********** Funciones Generales **********/
		function getDate(timestamp) {
			return CommonFactory.timestampToDate(timestamp);
		}
		
		function setHeaders(document, REPOCODE) {
			let today = getDate(new Date().getTime());
			document.addImage(empresas.TP.LOGO, 'PNG', 15, 15, 40, 32);
			document.setFontSize(14);
			document.text("Sistema de Gestión «Shipper»", 70, 20);
			document.setTextColor(32, 32, 32);
			document.setFontSize(10);
			document.text(`Empresa: ${empresas.TP.NAME}`, 70, 25);
			document.text(`RIF: ${empresas.TP.ID}`, 70, 30);
			document.setTextColor(96, 125, 139);
			document.text(`Fecha: ${today}`, 70, 35);
			if(REPOCODE)
				document.text(`Código de Solicitud: ${REPOCODE}`, width-75, 35);
			document.line(50, 50, width-25, 50);
			return document;
		}
		
		function setFooter(data, REPOCODE) {
			const totalPagesExp = "{total_pages_count_string}";
			let page = "";
			doc = setHeaders(doc, REPOCODE);
			if(typeof doc.putTotalPages === 'function')
				page = "Página "+data.pageCount;
			doc.setFontSize(8);
			doc.setTextColor(90 , 90, 90);
			centerText(`Dirección: ${empresas.TP.LOCATION}`, height-25);
			centerText(`Correo electrónico: ${empresas.TP.CONTACT.EMAIL1} / ${empresas.TP.CONTACT.EMAIL2}`, height-20);
			centerText(`Números de contacto: ${empresas.TP.CONTACT.TLF1} / ${empresas.TP.CONTACT.TLF2}`, height-15);
			doc.text(page, data.settings.margin.left, height-10);
		}
		
		function centerText(text, Y_POS) {
			let offset = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
			let X_POS = (doc.internal.pageSize.width - offset) / 2;
			doc.text(X_POS, Y_POS, text);
		}
		
		function printUserData(data, Y_POS) {
			doc.setFontType("bold");
			doc.text("Reporte solicitado por:", width-100, Y_POS);
			doc.setFontType("normal");
			doc.text(`Nombre: ${data.name}`, width-100, Y_POS+5);
			doc.text(`Cédula: ${data.id.type}-${data.id.number}`, width-100, Y_POS+10);
			doc.text(`Correo electrónico: ${data.email}`, width-100, Y_POS+15);
			doc.text(`Número de teléfono: ${data.phone.preffix}-${data.phone.number}`, width-100, Y_POS+20);
		}
		
		function getOptions() {
			return {
				"theme": 'striped',
				"tableWidth": 'auto',
				"styles": {
					"fontSize": 9,
					"font": 'Arial',
					"lineWidth": 0.5,
					"overflow": 'linebreak',
					"valign": 'middle',
					"halign": 'left'
				},
				"margin": {"top": 70},
				"columnStyles": {
					"id": {"fillColor": 99},
					"lineWidth": 2,
					"fontSize": 15,
					"font": 'Arial',
					"valign": 'middle',
					"halign": 'left',
					"overflow": 'linebreak'
				},
				"headerStyles": {"fillColor": [0, 74, 136]},
			};
		}
		/******************************************/
	}
})();