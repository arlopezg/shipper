(function () {
	'use strict';
	
	angular
	.module('shipper.reports')
	.controller('ReportsController', controller);
	
	controller.$inject = ["ReportsFactory", "ProductsFactory", "UsersFactory", "ServicesFactory", "ContactFactory"];
	
	function controller(ReportsFactory, ProductsFactory, UsersFactory, ServicesFactory, ContactFactory) {
		/*********** Variables *******************/
		let vm = this;
		vm.reports = [
			{
				"id": 1,
				"name": "Reporte general de Usuarios",
				"style": {"background": "#DF9262", "button": "#B86C3D", "text": "#FFFFFF"},
				"function": reporteUsuario,
			},
			{
				"id": 2,
				"name": "Reporte general de Productos",
				"style": {"background": "#DFB062", "button": "#B88A3D", "text": "#FFFFFF"},
				"function": reporteProductos,
			},
			{
				"id": 3,
				"name": "Reporte general de Servicios",
				"style": {"background": "#486793", "button": "#2C4D79", "text": "#FFFFFF"},
				"function": reporteServicios,
			},
			/*{
				"id": 4,
				"name": "Reporte general de Comercialización",
				"style": {"background": "#3F907E", "button": "#277765", "text": "#FFFFFF"},
				"function": reporteGeneralComercializacion,
			},
			*/
			{
				"id": 5,
				"name": "Reporte detallado de Comercialización",
				"style": {"background": "#3F907E", "button": "#277765", "text": "#FFFFFF"},
				"function": reporteDetalladoComercializacion,
			},
		];
		/*****************************************/
		
		/*********** Menu de funciones ***********/
		/*****************************************/
		
		/*********** Procesos ********************/
		/*****************************************/
		
		/***********Funciones especificas ********/
		/*****************************************/
		
		/*********** Funciones Generales **********/
		function reporteUsuario(from, to) {
			let users = UsersFactory.getAll();
			users.$loaded().then(() => {ReportsFactory.usersReport(users)});
		}
		
		function reporteProductos(from, to) {
			let products = ProductsFactory.getAll();
			products.$loaded().then(() => {ReportsFactory.productsReport(products)});
		}
		
		function reporteServicios(from, to) {
			let services = ServicesFactory.getAll();
			services.$loaded().then(() => {ReportsFactory.servicesReport(services)});
		}
		
		function reporteGeneralComercializacion(from, to) {
			let marketing = ContactFactory.getAll();
			marketing.$loaded().then(() => {ReportsFactory.marketingReportGeneral(marketing)});
		}
		
		function reporteDetalladoComercializacion(from, to) {
			let marketing = ContactFactory.getAll();
			marketing.$loaded().then(() => {ReportsFactory.marketingReportDetailed(marketing)});
		}
		
		/*****************************************/
	}
})();