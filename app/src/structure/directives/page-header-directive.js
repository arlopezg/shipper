/**
 * @desc
 * @example <page-header text="" color="" sideButton=""></page-header>
 */
angular
.module('shipper.products')
.directive('pageHeader', directive);

directive.$inject = [];

function directive() {
	let vm = this;
	let directive;
	
	directive = {
		restrict: "E",
		scope: {
			"text": "=",
			"color": "=",
			"sideButton": "=",
			"linkTo": "=",
		},
		templateUrl: "src/structure/directives/page-header-directive.html",
		controller: "",
		controllerAs: "vm"
	};
	
	return directive;
}