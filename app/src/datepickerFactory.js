(function () {
  'use strict';
  angular
  .module("shipper.site")
  .factory("DatepickerFactory", factory);
  
  factory.$inject = [];
  
  function factory() {
    /*********** Variables *******************/
    /*****************************************/
    
    /*********** Menu de funciones ***********/
    /*****************************************/
    
    /*********** Procesos ********************/
    return {
    	setMaxDateToday: setMaxDateToday,
    	setMinDayToday: setMinDayToday,
	    setOptions: setOptions,
    };
    /*****************************************/
    
    /***********Funciones especificas ********/
    function setMaxDateToday($dates) {
	    $dates.filter((date) => {return date.localDateValue() > new Date()}).map((date) => {date.selectable = false});
    }
    
    function setMinDayToday($dates) {
	    $dates.filter((date) => {return date.localDateValue() < new Date()}).map((date) => {date.selectable = false});
    }
    
    function setOptions() {
	    return {
		    "dropdownSelector":".dropdown-toggle",
		    "modelType": "DD-MM-YYYY",
		    "minView": "day"
	    };
    }
    /*****************************************/
    
    /*********** Funciones Generales **********/
    /*****************************************/
  }
})();
