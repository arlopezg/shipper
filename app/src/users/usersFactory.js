(function () {
  'use strict';
  angular
  .module('shipper.users', [])
  .factory('UsersFactory', UsersFactory);
  
  UsersFactory.$inject = ['FirebaseFactory', '$firebaseObject', '$firebaseArray',
    '$firebaseAuth', '$localStorage', "CommonFactory", "AuditFactory", "$state"];
  
  function UsersFactory(FirebaseFactory, $firebaseObject, $firebaseArray,
                        $firebaseAuth, $localStorage, CommonFactory, AuditFactory, $state) {
    /*********** Variables *******************/
    let auth = $firebaseAuth();
    let ref_usuarios = FirebaseFactory.getRef_usuarios();
    let ref_customers = FirebaseFactory.getRef_clientes();
    /*********** Fin de Variables ************/
    
    /*********** Menu de funciones ***********/
    /*********** Fin menu de funciones********/
    
    /*********** Procesos ********************/
    listenForAuthChanges();
    
    return {
      getAll: getAll,
      getAuthData: getAuthData,
      getUserById: getUserById,
      getTechnicians: getTechnicians,
      createUser: createUser,
      updateUser: updateUser,
      deleteUser: deleteUser,
      logInUser: logInUser,
      logOutUser:logOutUser,
    };
    /*********** Fin de process **************/
    
    /***********Funciones especificas ********/
    function startUser(uid) {
      $localStorage.user = getUserById(uid);
    }
    
    function endUser() {
      delete $localStorage.user;
    }
    
    function getAll() {
      return $firebaseArray(ref_usuarios);
    }
    
    function getUserById(uid) {
      return $firebaseObject(ref_usuarios.child(uid));
    }
    
    function getTechnicians() {
      // should only look up for users WHERE role = "technician"
    }
    
    function getAuthData(){
      if($localStorage.user)
        return $localStorage.user;
      else
        return false;
    }
    
    function createUser(user, callback){
      auth.$createUserWithEmailAndPassword(user.email, user.password)
      .then((data)=> {
        let password = user.password;
        delete user.password;
        delete user.password2;
        delete user.ToS;
        user.uid = data.uid;
        user.registerDate = new Date().getTime();
        user.role = "client";
        ref_usuarios.child("/"+data.uid).set(user)
        .then((data) => {
          CommonFactory.displayMessage("Registro exitoso", "success");
          logInUser(user.email, password);
        })
        .catch((error) => {
          CommonFactory.displayMessage("Ha ocurrido un error", "danger");
        });
      })
      .catch((error) => {
        switch (error.code){
          case "auth/email-already-in-use":
            CommonFactory.displayMessage("Error: el correo indicado ya pertenece a otro usuario", "danger");
            break;
          case "auth/invalid-email":
            CommonFactory.displayMessage("Error: el correo indicado no puede ser procesado", "danger");
            break;
          default:
            CommonFactory.displayMessage("Error desconocido", "danger", error);
            break;
        }
      });
    }
    
    function logInUser(email, password, callback){
      auth.$signInWithEmailAndPassword(email, password)
      .then((data)=> {
        // auth.useDeviceLanguage();
	      if(callback)
		      callback(true);
	      $state.go("site");
        CommonFactory.displayMessage("¡Bienvenido!", "success");
      })
      .catch((error)=> {
        if(callback)
          callback(false)
      });
    }
    
    function logOutUser(callback){
      auth.$signOut()
      .then((data)=>{
        endUser();
        callback(true);
      })
      .catch((error)=>{callback(error)});
    }
    
    function updateUser(id, user, callback) {
      let usuario = getUserById(id);
      usuario.$loaded().then(() => {
        ref_usuarios.child(id).update({"role": user.role})
        .then((data) => {
          CommonFactory.displayMessage("Modificación exitosa", "success");
          setAudit(`changed user '${id}' role to '${user.role}'`);
          if(callback)
            callback(true);
        })
        .catch((error) => {
          CommonFactory.displayMessage("Error al modificar usuario", "danger")});
      });
    }
    
    function deleteUser(callback, user) {
      $firebaseObject(ref_usuarios.child(user.$id)).$remove()
      .then((response)=>{
        callback(true);
      })
      .catch((error)=>{
        callback(false);
      });
    }
    
    function listenForAuthChanges() {
      firebase.auth().onAuthStateChanged((user) => {
        if (user)
          startUser(user.uid);
        else
          endUser();
      });
    }
    
    function setAudit(action) {
      AuditFactory.setAudit(action);
    }
    /***********Fin de funciones generales*****/
  }
})(firebase.auth());
