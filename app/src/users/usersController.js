/**
 * Created by EddieM on 09/05/2017.
 */
(function (firebase, fireAuth) {
  'use strict';

  angular
    .module('shipper.users')
    .controller('UsersController', controller);

  controller.$inject = ['UsersFactory', '$filter', 'CommonFactory', 'ContactFactory', '$timeout', 'ReportsFactory'];
  
  function controller(UsersFactory, $filter, CommonFactory, ContactFactory, $timeout, ReportsFactory) {
    /*********** Variables *******************/
    let vm = this;
    vm.userList = [];
    vm.user = {};
    vm.regexes = CommonFactory.regexes();
    vm.errorMessage = "";
    vm.logged = {};
    /*********** Fin de Variables ************/

    /*********** Menu de funciones ***********/
    vm.createUser  = createUser;
    vm.updateUser  = updateUser;
    vm.deleteUser  = deleteUser;
    vm.toggleModal = toggleModal;
    vm.print = print;
    vm.decimals = CommonFactory.thousandSeparator;
    /*********** Fin menu de funciones********/


    /*********** Procesos ********************/
    logged();
    /*********** Fin de process **************/


    /***********Funciones especificas ********/
    function createUser(user) {
      user.registerDate = new Date().getTime();
      user.role = "user";
      if(passwords_match(user.password, user.password_repeat))
        UsersFactory.createUser(createCallback, user);
      else
        vm.errorMessage = "Passwords don't match";
    }

    function updateUser(user) {
      UsersFactory.updateUser(callback, user.$id, user);
      toggleModal();
    }
   
    function deleteUser(user) {
      // factory uses user.$id
      UsersFactory.deleteUser(callback, user);
    }
    
    function toggleModal(user) {
      /*needs validation messages*/
      vm.user = (user)? angular.copy(($filter('filter')(vm.userList, {'$id':user.$id}))[0]):false;
      $('#userModal').modal('toggle');
    }
    
    function logged() {
	    vm.logged = UsersFactory.getAuthData();
	    $timeout(()=>{vm.logged.myRequests = ContactFactory.getRequestsByUser(vm.logged.email)}, 5000);
	    return vm.logged;
    }
    
    function print(req) {
    	let subtotal = 0;
    	req.data.map((item) => {subtotal += item._quantity * item._price});
    	let calculos = {
    		"subtotal": subtotal,
		    "IVA": (subtotal>2000000) ? 0.07:0.09,
    		"total": req.amount,
	    };
	    ReportsFactory.cartReport(req.data, req.sentDate, calculos, logged());
    }
    /*********** Fin Funciones especificas ***/

    /*********** Funciones Generales **********/
    function callback(data) {
      console.log("General callback:", data);
    }
    
    function passwords_match(password, password_repeat) {
      return password === password_repeat;
    }
  
    function createCallback(data) {
      if(!data)
        vm.errorMessage = data.message;
      else {
        vm.user = {};
        $('#userModal').modal('hide');
      }
    }
    /*********** Fin de funciones generales ***/
  }
})(firebase.database(), firebase.auth());
