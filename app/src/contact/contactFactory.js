(function() {
	'use strict';
	angular
	.module('shipper.site', [])
	.factory('ContactFactory', factory);
	
	factory.$inject = ['FirebaseFactory', 'CommonFactory', '$firebaseObject', '$firebaseArray', "AuditFactory",
		"UsersFactory"];
	
	function factory(FirebaseFactory, CommonFactory, $firebaseObject, $firebaseArray, AuditFactory,
	                 UsersFactory) {
		/*********** Variables *******************/
		let ref_contact  = FirebaseFactory.getRef_contacto();
		let ref_newsletter  = FirebaseFactory.getRef_newsletter();
		let CURRENT_USER = FirebaseFactory.getCurrentAuth();
		let push;
		/*********** Fin de Variables ************/
		
		/*********** Menu de funciones ***********/
		/*********** Fin menu de funciones********/
		
		/*********** Procesos ********************/
		return {
			getAll:            getAll,
			// getPendingMessages: getPendingMessages,
			getMessageById:    getMessageById,
			getRequestsByUser: getRequestsByUser,
			getRequestsByTech: getRequestsByTech,
			sendMessage:       sendMessage,
			replyToMessage:    replyToMessage,
			setVisit:          setVisit,
			approve:           approve,
			addToNewsletter:   addToNewsletter,
		};
		
		/*********** Fin de process **************/
		
		/***********Funciones especificas ********/
		function sendMessage(message, callback, data) {
			push             = ref_contact.push();
			message.id       = push.key;
			message.status   = false;
			message.sentDate = new Date().getTime();
			if(data)
				message.data = data;
			ref_contact.child(`/${push.key}`).set(message)
			.then((data) => {
				if(callback)
					callback(true);
				CommonFactory.displayMessage("Mensaje enviado con éxito", "success");
			})
			.catch((error) => {CommonFactory.displayMessage("Error al enviar mensaje", "danger")});
		}
		
		function replyToMessage(id, reply, status, callback) {
			ref_contact.child(`/${id}`).update({"status": status, "visit": reply})
			.then(() => {
				if(callback)
					callback(true);
				setAudit(`setted request '${id}' to status '${status}'`);
				CommonFactory.displayMessage("Se asignó una respuesta al mensaje", "success");
			})
			.catch(() => {
				if(callback)
					callback(false);
				CommonFactory.displayMessage("Error al enviar mensaje", "danger");
			});
		}
		
		function getAll() {
			return $firebaseArray(ref_contact);
		}
		
		function getRequestsByUser(email) {
			return $firebaseArray(ref_contact.orderByChild("author/email").equalTo(email));
		}
		
		function getRequestsByTech(id) {
			console.log(id);
			return $firebaseArray(ref_contact.orderByChild("visit/techId").equalTo(id));
		}
		
		function getMessageById(id) {
			return $firebaseObject(ref_contact.child(id));
		}
		
		function setVisit(msgId, data, callback) {
			let message = getMessageById(msgId);
			message.$loaded().then(() => {
				ref_contact.child(msgId).update({"status": "Planificación", "visit": data})
				.then(() => {
					if(callback)
						callback(true);
					setAudit(`setted visit '${msgId}' user to tech '${data.techId}'`);
					CommonFactory.displayMessage("Visita planificada", "success");
				})
				.catch((error) => {
					if(callback)
						callback(false);
					console.log(error);
					setAudit(`error while trying to set visit '${msgId}' user to tech '${data.techId}'`);
					CommonFactory.displayMessage("Error al planificar visita", "danger");
				});
			});
		}

		function approve(msgId, status, callback) {
			let message = getMessageById(msgId);
			message.$loaded().then(() => {
				ref_contact.child(msgId).update({"status": status})
				.then((data) => {
					if(callback)
						callback(true);
					setAudit(`setted request '${msgId}' status to '${status}'`);
					CommonFactory.displayMessage("Visita aprobada", "success");
				})
				.catch((error) => {
					if(callback)
						callback(false);
					setAudit(`error while trying to set request '${msgId}' status to '${status}'`);
					CommonFactory.displayMessage("Error al aprobar visita", "danger");
				});
			});
		}
		
		function addToNewsletter(email, callback) {
			push             = ref_newsletter.push();
			ref_newsletter.child(`/${push.key}`).set(email)
			.then((data) => {
				if(callback)
					callback(true);
				CommonFactory.displayMessage("Suscripción exitosa", "success");
			})
			.catch((error) => {
				if(callback)
					callback(false);
				CommonFactory.displayMessage("Error al suscribirte", "danger");
			});
		}
		
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		function setAudit(action) {
			AuditFactory.setAudit(action);
		}
		
		/***********Fin de funciones generales*****/
	}
})();