(function() {
	'use strict';
	angular
	.module('shipper.site')
	.controller('ContactController', controller);
	
	controller.$inject = ['ContactFactory', "CommonFactory", "UsersFactory", "DatepickerFactory"];
	
	function controller(ContactFactory, CommonFactory, UsersFactory, DatepickerFactory) {
		/*********** Variables *******************/
		let vm                   = this;
		vm.regexes               = CommonFactory.regexes();
		vm.options               = DatepickerFactory.setOptions();
		vm.startDateBeforeRender = DatepickerFactory.setMinDayToday;
		/*****************************************/
		
		/*********** Menu de funciones ***********/
		vm.sendMessage = sendMessage;
		vm.getAll            = getAll;
		vm.thousandSeparator = CommonFactory.thousandSeparator;
		vm.getMessageData    = getMessageData;
		vm.getTechnicians    = getTechnicians;
		vm.setVisit          = setVisit;
		vm.reply          = reply;
		/*****************************************/
		
		/*********** Procesos ********************/
		getAll();
		getTechnicians();
		/*****************************************/
		
		/***********Funciones especificas ********/
		function sendMessage(message) {
			ContactFactory.sendMessage(message, result);
			function result(data) {
				if(data)
					vm.lock = true;
			}
		}
		
		function getAll() {
			vm.messages = ContactFactory.getAll();
		}
		
		function getMessageData(message, disable) {
			vm.message      = angular.copy(message);
			vm.disableModal = !!disable;
			if(!!message.data)
				vm.message.has = {
					"services": message.data.some(item => item._data.type === "service"),
					"products": message.data.some(item => item._data.type === "product")
				};
			else
				vm.message.has = false;
		}
		
		function getTechnicians() {
			vm.technicians = UsersFactory.getAll();
		}
		
		function setVisit(msgId, data) {
			ContactFactory.setVisit(msgId, data, result);
			function result(data) {
				if(data) {
					vm.planif = {};
					$("#messageDataModal").modal("toggle");
				}
			}
		}
		
		function reply(msgId, status) {
			ContactFactory.approve(msgId, status, result);
			function result(data) {
				if(data) {
					vm.planif = {};
					$("#messageDataModal").modal("toggle");
				}
			}
		}
		/*****************************************/
		
		/*********** Funciones Generales **********/
		/*****************************************/
	}
})();