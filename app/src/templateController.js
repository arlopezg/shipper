(function () {
  'use strict';
  
  angular
  .module('shipper.site')
  .controller('templateController', controller);
  
  controller.$inject = [];
  
  function controller() {
    /*********** Variables *******************/
    let vm = this;
    /*****************************************/
    
    /*********** Menu de funciones ***********/
    /*****************************************/
    
    /*********** Procesos ********************/
    /*****************************************/
    
    /***********Funciones especificas ********/
    /*****************************************/
  
    /*********** Funciones Generales **********/
    /*****************************************/
  
  }
})();
