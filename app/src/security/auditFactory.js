(function () {
	'use strict';
	angular
	.module('shipper.security', [])
	.factory('AuditFactory', factory);
	
	factory.$inject = ['FirebaseFactory', '$localStorage'];
	
	function factory(FirebaseFactory, $localStorage) {
		/*********** Variables *******************/
		let ref_audit = FirebaseFactory.getRef_audit();
		/*********** Fin de Variables ************/
		
		/*********** Menu de funciones ***********/
		/*********** Fin menu de funciones********/
		
		/*********** Procesos ********************/
		return {
			setAudit: setAudit,
			getLogs: getLogs,
		};
		/*********** Fin de process **************/
		
		/***********Funciones especificas ********/
		function setAudit(action) {
			let push;
			let log = {"action": action, "user": $localStorage.user.uid, "time": new Date().getTime()};
			push = ref_audit.push();
			log.id = push.key;
			ref_audit.child(`/${push.key}`).set(log);
		}
		
		function getLogs() {
			return $firebaseArray(ref_audit);
		}
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		/***********Fin de funciones generales*****/
	}
})();
