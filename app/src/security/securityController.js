(function () {
	'use strict';
	
	angular
	.module('shipper.security')
	.controller('SecurityController', controller);
	
	controller.$inject = ["UsersFactory", "CommonFactory", "$state", "FirebaseFactory", "CartFactory",
		"$rootScope", "$stateParams", "$timeout", "DatepickerFactory", "$translate"];
	
	function controller(UsersFactory, CommonFactory, $state, FirebaseFactory, CartFactory,
	                    $rootScope, $stateParams, $timeout, DatepickerFactory, $translate) {
		/*********** Variables *******************/
		let vm = this;
		vm.user = {};
		vm.regexes = CommonFactory.regexes();
		vm.preffixesList = CommonFactory.phonePreffixesList();
		vm.identityDocumentsInfo = CommonFactory.identityDocumentsInfo();
		vm.howManyItems = [];
		vm.loginMessage = "";
		vm.options = DatepickerFactory.setOptions();
		vm.startDateBeforeRender = DatepickerFactory.setMaxDateToday;
		/*****************************************/
		
		/*********** Menu de funciones ***********/
		vm.createUser = createUser;
		vm.goToHome = goToHome;
		vm.isLogged = isLogged;
		vm.logIn = logIn;
		vm.logOut = logOut;
		vm.resetPassword = resetPassword;
		vm.setLang = setLang;
		/*****************************************/
		
		/*********** Procesos ********************/
		howManyItems();
		listenToCart();
		CommonFactory.preventCollapseOnDesktop();
		/*****************************************/
		
		/***********Funciones especificas ********/
		function logIn(user){
			UsersFactory.logInUser(user.email, user.password, redirect);
			function redirect(data) {
				switch(true) {
					case !!data && !!$stateParams.goBackTo:
						$state.go($stateParams.goBackTo);
						break;
					case !!data && !$stateParams.goBackTo:
						goToHome();
						break;
					case !data:
						CommonFactory.displayMessage("Datos inválidos", "danger");
						break;
				}
			}
		}
		
		function createUser(user) {
			if(match(user.password, user.password2))
				UsersFactory.createUser(user);
		}
		
		function logOut(){
			UsersFactory.logOutUser(() => {$state.go("site")});
		}
		
		function isLogged() {
			return UsersFactory.getAuthData();
		}
		
		function resetPassword(user) {
			FirebaseFactory.resetPassword(user.email, result);
			function result(data, cause) {
				if(!!data) {
					vm.errorText = false;
					vm.resetText = "Operación exitosa. Serás redirigido pronto...";
					$timeout(function() {$state.go("auth.login")}, 3000);
				} else {
					switch(true) {
						case cause.code === "auth/internal-error":
							vm.errorText = "se alcanzó el límite de solicitudes";
							break;
						case cause.code === "auth/user-not-found":
							vm.errorText = "usuario no encontrado";
							break;
						case cause.code === "auth/invalid-email":
							vm.errorText = "correo electrónico inválido";
							break;
						default:
							vm.errorText = "ha ocurrido un error";
							break;
					}
				}
			}
		}
		
		function goToHome() {
			$state.go("site");
		}
		
		function setLang(lang) {
			$translate.use(lang);
		}
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		function callback(data) {
			console.log("[SecurityController]", data);
		}
		
		function match(password, password2) {
			return (password === password2);
		}
		
		function howManyItems() {
			vm.howManyItems = CartFactory.howManyItems();
		}
		
		function listenToCart() {
			$rootScope.$on("ngCart:change", function () {
				howManyItems();
			})
		}
		/*********** Fin de funciones generales ***/
	}
})();
