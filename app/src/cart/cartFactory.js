(function () {
	'use strict';
	angular
	.module('shipper.cart', [])
	.factory("CartFactory", factory);
	
	factory.$inject = ["ngCart", "ngCartItem", "$rootScope", "CommonFactory"];
	
	function factory(ngCart, ngCartItem, $rootScope, CommonFactory) {
		/*********** Variables *******************/
		/*********** Fin de Variables ************/
		
		/*********** Menu de funciones ***********/
		/*********** Fin menu de funciones********/
		
		/*********** Procesos ********************/
		listenForAddedItems();
		listenForRemovedItems();
		
		return {
			addItem: addItem,
			empty: empty,
			getItemById: getItemById,
			getItems: getItems,
			howManyItems: howManyItems,
			modifyQuantity: modifyQuantity,
			removeItem: removeItem,
		};
		/*********** Fin de process **************/
		
		/***********Funciones especificas ********/
		function getItems() {
			return ngCart.getItems();
		}
		
		function getItemById(id) {
			return ngCart.getItemById(id);
		}
		
		function modifyQuantity(id, action) {
			let item = getItemById(id);
			if(action && (item.getQuantity()<item._data.stock.quantity))
				item.setQuantity(1, true);
			else if(!action && item.getQuantity()>1)
				item.setQuantity(-1, true);
			$rootScope.$broadcast("ngCart:change");
		}
		
		function howManyItems() {
			return Number(ngCart.getItems().length);
		}
		
		function addItem(id, name, price, quantity, options, type) {
			let data;
			if(type === "service")
				data = {"description": options.description, "type": "service"};
			else if(type === "product")
				data = {
					"type": "product",
					"stock": {"quantity": options.stock.quantity, "type": options.stock.type},
					"description": options.description
				};
			else
				data = {"type": "other"};
			ngCart.addItem(id, name, price, quantity, data);
		}
		
		function removeItem(id) {
			let items = getItems();
			ngCart.removeItem(CommonFactory.findIndexInArray(items, "_id", id));
		}
		
		function empty() {
			ngCart.empty();
			$rootScope.$broadcast("ngCart:change");
		}
		/******************************************/
		
		/*********** Funciones Generales **********/
		function listenForAddedItems() {
			$rootScope.$on("ngCart:itemAdded", function() {
				CommonFactory.displayMessage("Artículo añadido al presupuesto", "success", false);
			});
		}
		
		function listenForRemovedItems() {
			$rootScope.$on("ngCart:itemRemoved", function() {
				CommonFactory.displayMessage("Artículo removido del presupuesto", "info");
			});
		}
		/******************************************/
	}
})();
