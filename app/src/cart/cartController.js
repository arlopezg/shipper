(function () {
  'use strict';
  
  angular
  .module('shipper.cart')
  .controller('CartController', controller);
  
  controller.$inject = ['CartFactory', 'CommonFactory', '$filter', 'ReportsFactory',
    'UsersFactory', 'ContactFactory', "$state"];
  
  function controller(CartFactory, CommonFactory, $filter, ReportsFactory,
                      UsersFactory, ContactFactory, $state) {
    /*********** Variables *******************/
    let vm = this;
    vm.items = [];
    vm.calculos = {
      "subtotal": 0,
      "total": 0,
      "IVA": 0.00
    };
    /*****************************************/
    
    /*********** Menu de funciones ***********/
    vm.thousandSeparator = CommonFactory.thousandSeparator;
    vm.modifyQuantity = modifyQuantity;
    vm.remove = CartFactory.removeItem;
    vm.viewDetails = viewDetails;
    vm.getSubtotal = getSubtotal;
    vm.getReport = getReport;
    vm.getAuthData = UsersFactory.getAuthData;
    vm.getTotal = getTotal;
    vm.clearCart = clearCart;
    // vm.getIVA = FirebaseFactory.getIVA;
    /*****************************************/
    
    /*********** Procesos ********************/
    getItems();
    CommonFactory.initTooltip();
    /*****************************************/
    
    /***********Funciones especificas ********/
    function getReport(items, calculos) {
      let today = new Date().getTime();
      let usuario = UsersFactory.getAuthData();
      let message = {
      	"message": "Solicitud de presupuesto",
	      "topic": "Presupuesto",
	      "amount": calculos.total,
	      "author": {"name": `${usuario.name} ${usuario.lastname}`, "email": usuario.email}
      };
      ReportsFactory.cartReport(items, today, calculos, vm.getAuthData());
	    ContactFactory.sendMessage(message, clearCart, items);
    }
    
    function viewDetails(item) {
      if(item._data.type === "product")
      	$state.go("site.products.details", {"productId": item._id});
	    else if(item._data.type === "service")
	    	$state.go("site.services.details", {"serviceId": item._id});
    }
    
    function modifyQuantity(id, action) {
      CartFactory.modifyQuantity(id, action);
      getSubtotal(vm.items);
    }
    
    function getSubtotal(items) {
      let subtotal = 0;
      items.map((item) => {
        subtotal += item._price*item._quantity;
      });
      vm.calculos.subtotal = subtotal;
      getIVA(subtotal);
      return subtotal;
    }
    
    function getTotal(amount, tax) {
      vm.calculos.total = amount+(amount*tax);
      return vm.calculos.total;
    }
    
	  function clearCart(data) {
		  if(data) {
				vm.items = [];
			  CartFactory.empty();
		  }
	  }
    /*****************************************/
    
    /*********** Funciones Generales **********/
    function getItems() {
      vm.items = CartFactory.getItems();
    }
    
    function getIVA(subtotal) {
      if(subtotal < 2000000)
        vm.calculos.IVA = 0.09;
      else
        vm.calculos.IVA = 0.07;
      getTotal(subtotal, vm.calculos.IVA);
      return vm.calculos.IVA;
    }
    /*****************************************/
  }
})();
