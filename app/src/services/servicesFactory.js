(function () {
	'use strict';
	angular
	.module('shipper.services', [])
	.factory('ServicesFactory', factory);
	
	factory.$inject = ['FirebaseFactory', '$firebaseObject', '$firebaseArray', 'CommonFactory',
		'$localStorage', "AuditFactory"];
	
	function factory(FirebaseFactory, $firebaseObject, $firebaseArray, CommonFactory,
	                 $localStorage, AuditFactory) {
		/*********** Variables *******************/
		const ref_servicios = FirebaseFactory.getRef_servicios();
    const ref_categories = FirebaseFactory.getRef_categories().child("services");
		let push;
		let producto;
		/*********** Fin de Variables ************/
		
		/*********** Menu de funciones ***********/
		/*********** Fin menu de funciones********/
		
		/*********** Procesos ********************/
		return {
			getAll: getAll,
			getServiceById: getServiceById,
			getCategories: getCategories,
			manageCategories: manageCategories,
			addService: addService,
			updateService: updateService,
			addToCart: addToCart,
			changeServiceStatus: changeServiceStatus
		};
		/*********** Fin de process **************/
		
		/***********Funciones especificas ********/
		function getAll() {
			return $firebaseArray(ref_servicios);
		}
		
		function getServiceById(uid) {
			return $firebaseObject(ref_servicios.child(uid));
		}
		
		function getCategories() {
			return $firebaseArray(ref_categories);
		}
	
	  function manageCategories(action, category, callback) {
	  	switch(true) {
	  		case action === "add":
	  		push = ref_categories.push();
			  category.id = push.key;
			  ref_categories.child(`/${category.id}`).set(category)
			  .then(() => {
				  CommonFactory.displayMessage("Categoría creada con éxito", "success");
				  setAudit(`created category '${category.name}'`);
			  })
			  .catch((info) => {console.log(info);CommonFactory.displayMessage("Error al crear categoría", "danger")});
	  		break;
	  		case action === "delete":
			  ref_categories.child(`/${category.id}`).remove()
			  .then(() => {
				  CommonFactory.displayMessage("Categoría eliminada con éxito", "success");
				  setAudit(`deleted category '${category.name}'`);
			  })
			  .catch((info) => {console.log(info);CommonFactory.displayMessage("Error al eliminar categoría", "danger")});
	  		break;
	  	}
	  }
		
		function addService(product) {
			push = ref_servicios.push();
			product.id = push.key;
			product.enabled = true;
			product.registerDate = new Date().getTime();
			ref_servicios.child("/"+push.key).set(product)
			.then((data) => {
				CommonFactory.displayMessage("Producto creado con éxito", "success");
			})
			.catch((data) => {
				CommonFactory.displayMessage("Error al crear producto", "danger");
			});
		}
		
		function changeServiceStatus(id, callback) {
			producto = getServiceById(id);
			producto.$loaded().then(() => {
				ref_servicios.child(producto.id).update({"enabled": !producto.enabled})
				.then((data) => {
					CommonFactory.displayMessage("Servicio actualizado", "success");
				})
				.catch((data) => {
					CommonFactory.displayMessage("Error al modificar servicio", "danger");
				});
			});
		}
		
		function updateService(id, product) {
			delete product.$id;
			delete product.$priority;
			producto = getServiceById(id);
			producto.$loaded().then(() => {
				ref_servicios.child(producto.id).update(product)
				.then((data) => {
					CommonFactory.displayMessage("Servicio actualizado", "success");
				})
				.catch((data) => {
					CommonFactory.displayMessage("Error al modificar servicio", "danger");
				});
			});
		}
		
		function addToCart(id, price) {
			let cart = $localStorage.cart;
			if(CommonFactory.findIndexByKeyValue(cart, "id", id)) {
				let index = cart.findIndex(item => item.id === id);
				cart.splice(index, 1);
			} else
				cart.push({"id": id, "price": price});
		}
	
	  function getCategories() {
		  return $firebaseArray(ref_categories);
	  }
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		function setAudit(action) {
	    AuditFactory.setAudit(action);
    }
		/***********Fin de funciones generales*****/
	}
})();
