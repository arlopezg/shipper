(function () {
	'use strict';
	
	angular
	.module('shipper.services')
	.controller('ServicesController', controller);
	
	controller.$inject = ['ServicesFactory', 'CommonFactory', '$state', 'CartFactory'];
	
	function controller(ServicesFactory, CommonFactory, $state, CartFactory) {
		/*********** Variables *******************/
		let vm = this;
		vm.service = {};
		vm.regexes = CommonFactory.regexes();
		vm.disableModal = false;
		/*****************************************/
		
		/*********** Menu de funciones ***********/
		vm.addService = addService;
		vm.changeServiceStatus = changeServiceStatus;
		vm.getServiceData = getServiceData;
		vm.getCategories = getCategories;
		vm.manageCategories = manageCategories;
		vm.setServiceData = setServiceData;
		vm.quickAddToCart = quickAddToCart;
		vm.quickRemoveFromCart = quickRemoveFromCart;
		vm.thousandSeparator = CommonFactory.thousandSeparator;
		vm.itemInCart = CartFactory.getItemById;
		/*****************************************/
		/*********** Procesos ********************/
		CommonFactory.initTooltip();
		getAll();
		getCategories();
		/*****************************************/
		
		/***********Funciones especificas ********/
		function addService(service) {
			ServicesFactory.addService(service);
		}
		
		function quickAddToCart(service) {
			CartFactory.addItem(service.id, service.name, service.price, 1, service, "service");
		}
		
		function quickRemoveFromCart(id) {
			CartFactory.removeItem(id);
		}
		
		function changeServiceStatus(id) {
			ServicesFactory.changeServiceStatus(id);
		}
		
		function getServiceData(service, disable) {
			// So it doesn't get updated on the go
			vm.service = angular.copy(service);
			// disable service form
			vm.disableServiceModal = !!disable;
		}
		
		function setServiceData(service) {
			if(service.id)
				ServicesFactory.updateService(service.id, service);
			else
				ServicesFactory.addService(service);
		}

		function manageCategories(action, data) {
			ServicesFactory.manageCategories(action, data);
		}
		/*********** Fin Funciones especificas ***/
		
		/*********** Funciones Generales **********/
		function callback(data) {
			console.log("[serviceController]",data);
		}
		
		function getAll() {
			vm.services = ServicesFactory.getAll();
		}

		function getCategories() {
			vm.categories = ServicesFactory.getCategories();
		}
		/*********** Fin de funciones generales ***/
	}
})();
