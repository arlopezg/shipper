
/**
 * @desc Lista de los diferentes productos
 * @example <services-list services="" name="" category="" order="" limit=""></services-list>
 */
angular
.module('shipper.services')
.directive('servicesList', directive);

directive.$inject = [];

function directive() {
	let vm = this;
	let directive;
	
	directive = {
		restrict: "E",
		scope: {
			"services": "=",
			"name": "=",
			"category": "=",
			"order": "=",
			"limit": "="
		},
		templateUrl: "src/services/directives/services-list-directive.html",
		controller: "ServicesController",
		controllerAs: "vm"
	};
	
	return directive;
}