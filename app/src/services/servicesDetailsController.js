/**
 * Created by Alejandro Lopez on 19/08/2017.
 */

(function () {
  'use strict';
  
  angular
  .module('shipper.services')
  .controller('ServicesDetailsController', controller);
  
  controller.$inject = ['ServicesFactory', 'CommonFactory', '$state', 'CartFactory'];
  
  function controller(ServicesFactory, CommonFactory, $state, CartFactory) {
    /*********** Variables *******************/
    let vm = this;
    vm.regexes = CommonFactory.regexes();
    /*****************************************/
    
    /*********** Menu de funciones ***********/
    vm.thousandSeparator = CommonFactory.thousandSeparator;
    vm.addToCart = addToCart;
    vm.removeItem = removeItem;
    vm.itemInCart = CartFactory.getItemById;
    /*****************************************/
    
    /*********** Procesos ********************/
    CommonFactory.initTooltip();
    getServiceData();
    /*****************************************/
    
    /***********Funciones especificas ********/
    function addToCart(service, quantity) {
      CartFactory.addItem(service.id, service.name, service.price, 1, service, "service");
    }
    
    function removeItem(id) {
      CartFactory.removeItem(id);
    }
    /*********** Fin Funciones especificas ***/
    
    /*********** Funciones Generales **********/
    function callback(data) {
      console.log(data);
    }
    
    function getServiceData() {
      vm.service = ServicesFactory.getServiceById($state.params.serviceId);
    }
    /*********** Fin de funciones generales ***/
  }
})();
